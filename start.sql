
CREATE TABLE IF NOT EXISTS config (
	id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
	path VARCHAR(64) NOT NULL,
	size INTEGER UNSIGNED NOT NULL DEFAULT 0,
	total SMALLINT UNSIGNED NOT NULL DEFAULT 0,
	config LONGTEXT NOT NULL,
	modification TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,

	PRIMARY KEY(id),
	KEY(path(32)),
	KEY(total),
	KEY(modification),
	KEY(size)
);

CREATE TABLE IF NOT EXISTS config_rules (
	id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
	id_config SMALLINT UNSIGNED NOT NULL,
	rule VARCHAR(32) NOT NULL COMMENT 'Regex to parse and respond accordingly to the sent content',
	response TEXT NOT NULL,
	mime_type VARCHAR(32),
	http_status VARCHAR(32) NOT NULL DEFAULT 'HTTP/1.1 Ok',

	PRIMARY KEY(id),
	KEY(id_config)
);

CREATE TABLE IF NOT EXISTS logs  (
	id SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
	id_config SMALLINT UNSIGNED NOT NULL,
	body TEXT NOT NULL,
	response LONGTEXT NOT NULL,
	headers LONGTEXT NOT NULL,
	cookies LONGTEXT NOT NULL,
	gets LONGTEXT NOT NULL,
	posts LONGTEXT NOT NULL,
	extra LONGTEXT NOT NULL,
	size INTEGER UNSIGNED NOT NULL DEFAULT 0,
	mime_type VARCHAR(48) NOT NULL DEFAULT 'text/plain',
	creation TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,

	PRIMARY KEY(id),
	KEY(id_config, id),
	KEY(creation)
);
