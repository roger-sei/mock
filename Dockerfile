FROM php:8.0.1-apache-buster

EXPOSE 80 8200

RUN mkdir -p /var/www/html \
	&& mkdir -p /var/lib/mysql/

COPY ./start.sql /var/lib/mysql/start.sql
COPY src /var/www/html
COPY scripts /var/www/scripts

RUN mkdir -p /var/www/html \
	&& mkdir -p /var/lib/mysql/ \
	&& a2enmod rewrite \
	&& a2enmod headers \
	&& apt update \
	&& apt install -y mariadb-server-10.3 \
	&& docker-php-ext-install pdo_mysql \
	&& echo '################## MYSQL ##################' \
	&& service mysql start \
	&& mysql -e 'CREATE DATABASE mock' \
	&& mysql -e "CREATE USER mock@localhost IDENTIFIED BY '1234'" \
	&& mysql -e "GRANT ALL PRIVILEGES ON mock.* TO 'mock'@localhost IDENTIFIED BY '1234'" \
	&& mysql mock < /var/lib/mysql/start.sql \
	&& service mysql start \
	&& apachectl start \
	&& /var/www/scripts/b2b.sh \
	&& echo '################## DONE ##################'

# Debugging
#VOLUME /var/www/html
#VOLUME /var/lib/mysql

WORKDIR /var/www/html

CMD service mysql start && apache2-foreground
