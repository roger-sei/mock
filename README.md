# Mock

Mock it's a very simple mock up testing tool, made simple with docker. You only need a single command and that's all.



### Usage

Run the following docker command:
```docker
docker run --name mock -p8200:80 -d rogersei/mock
```

You can now access your browser in the following address [localhost:8200](http://localhost:8200/) which is listening for incoming data.

To send data, use your favorite tool, like:
```bash
curl -X POST -d 'hello world!!!' http://localhost:8200/my-first-post
```



### Navigation

Mostly actions is done using keyboard shortcuts:
- **H**ome
- **P**ause or resume log updates (log page)
- **D**eletes the whole log (log page)
- **←** Move one tab left (log page)
- **→** Move one tab right (log page)
- **↑** Go to previous log (log page)
- **↓** Go to next log (log page)
- **?** Open Help menu
