<?php

error_reporting(E_ALL);
ini_set('display_errors', 'On');
define('ROOT', dirname(__DIR__));
date_default_timezone_set('America/Sao_Paulo');

$GLOBALS['tstart'] = microtime(true);

require_once 'Main.php';


(new class extends Mock\Core\Main {


    /**
     * Route dispatcher.
     */
    public function __construct()
    {
        $this->systemCheckup();
        $this->loadIniConstants();
        $this->setAutoLoad();
        $page = ($_GET['--mock--route-page'] ?? '');
        unset($_GET['--mock--route-page']);
        unset($_REQUEST['--mock--route-page']);
        $this->parseRoutes($page);
    }


    /**
     * Load defined constants in config/constants.ini.
     *
     * @return void.
     */
    private function loadIniConstants(): void
    {
        foreach (parse_ini_file(ROOT . '/config/constants.ini') as $name => $value) {
            define($name, $value);
        }
    }


    /**
     * Basic checkups.
     *
     * @return void.
     */
    private function systemCheckup(): void
    {
        
    }


    /**
     * Register for auto load.
     *
     * @return void.
     */
    protected function setAutoLoad(): void
    {
        spl_autoload_register(function($className) {
            $parts = explode('\\', $className);
            $sroot = ROOT;

            if ($parts[0] === 'Mock') {
                array_shift($parts);
            }

            $classParsed = join('/', $parts);

            $this->dieIfFail(file_exists("{$sroot}/{$classParsed}.php") === false, "Class '$classParsed' not found");
            include_once "$sroot/$classParsed.php";
        });
    }


});


