<?php
/**
 * Services manager.
 *
 * @category  PHP
 * @package   GT8
 * @author    GT8 <roger.sei@icloud.com>
 * @copyright 2021 GT8
 * @license   /license GPL2
 * @version   Release: GIT 1
 * @link      /services/
 */

namespace Mock\Core;

class Services
{

    /**
     * Parameters sent from payload.
     *
     * @var array.
     */
    public static $args = [];


    /**
     * Returns the internal HTTP Rest method.
     *
     * @return string.
     */
    public static function getMethod(): string
    {
        $method   = strtolower($_REQUEST['_method'] ?? $_SERVER['REQUEST_METHOD'] ?? 'get');
        $alloweds = [
            'get',
            'post',
            'patch',
            'put',
            'delete',
        ];

        if (in_array($method, $alloweds) === false) {
            $method = 'invalid';
        }

        return $method;
    }

    
    /**
     * Returns an invalid response.
     *
     * @return array.
     */
    public static function invalid(): array
    {
        return ([
            'error' => 'Not implemented',
        ]);
    }


    /**
     * Auto run for Services.
     * 
     * @return void.
     */
    public function run()
    {
        parse_str(file_get_contents('php://input'), $args);
        self::$args = $args;

        $method  = $this->getMethod();
        $results = $this->$method();

        Main::finish($results);
    }


}
