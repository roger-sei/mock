<?php

namespace Mock\Core;


class Util
{


    /**
     * Transforms a number into a friendly readable formatm with suffixes like MB, GB, etc.
     *
     * @param integer $size A number to be converted in a human friendly format.
     *
     * @return string.
     */
    public static function integerToBytes(int $size): string
    {
        $result = '';
        if ($size > pow(1024, 3)) {
            $result = number_format($size / pow(1024, 3), 2) . ' GB';
        } elseif ($size > pow(1024, 2)) {
            $result = number_format($size / pow(1024, 2), 2) . ' MB';
        } elseif ($size > 1024) {
            $result = number_format($size / 1024, 2) . ' KB';
        } else {
            $result = "$size B";
        }

        return $result;
    }


    /**
     * Helper para expressão regular.
     *
     * @param string $value Texto.
     * @param string $reg   Expressão regular.
     *
     * @return string.
     */
    public static function regExp(string $value, string $reg): string
    {
        preg_match("/$reg/", $value, $matches);
        return ($matches[1] ?? $matches[0] ?? '');
    }
    

    /**
     * Parse and transform a given string in a json format, with [name=>value].
     *
     * @param string $value     Required string to be converted.
     * @param string $delimiter Array delimiter. Default ";"
     *
     * @return array.
     */
    public static function parseProperties(string $value, string $delimiter = ';'): array
    {
        $array = [];
        $value = str_replace("\\$delimiter", '##DELIMITER-PARSER##', $value);
        $obj   = explode($delimiter, $value);
        foreach ($obj as $value) {
            if (strpos($value, '=') !== false) {
                $name  = trim(substr($value, 0, strpos($value, '=')));
                $value = trim(substr($value, (strpos($value, '=') + 1)));
                $value = str_replace('##DELIMITER-PARSER##', $delimiter, $value);
            } else {
                $name  = trim(str_replace('##DELIMITER-PARSER##', $delimiter, $value));
                $value = true;
            }

            $array[$name] = $value;
        }

        return $array;
    }


    /**
     * Transforms a unix timestamp in an ellapsed time.
     *
     * @param integer $time Unix timestamp.
     *
     * @return string.
     */
    public static function secondsToEllapsed(int $time): string
    {
        if (($t = ($time / (60 * 60 * 24 * 30))) > 1) {
            $ellapsed = ($t < 2 ? "1 month" : floor($t) . ' months' );
        } elseif (($t = ($time / (60 * 60 * 24))) > 1) {
            $ellapsed = ($t < 2 ? "1 day" : floor($t) . ' days' );
        } elseif (($t = ($time  / (60 * 60))) > 1) {
            $ellapsed = ($t < 2 ? "1 hour" : floor($t) . ' hours' );
        } elseif (($t = ($time / (60))) > 1) {
            $ellapsed = ($t < 2 ? "1 minute" : floor($t) . ' minutes' );
        } else {
            $ellapsed = $time . ' seconds';
        }

        return $ellapsed;
    }


}
