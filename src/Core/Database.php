<?php
/**
 * Singleton for DAtabase connection, using PDO.
 *
 * PHP Version 8.0
 *
 * @category  PHP
 * @package   Core
 * @author    Roger Sei <roger.sei@icloud.com>
 */

namespace Mock\Core;


class Database extends \PDO
{

    /**
     * @var boolean.
     */
    private $hasTransaction = false;

    /**
     * @var array.
     */
    public static $instance = null;

    /**
     * @var mixed.
     */
    private $stmt = null;


	/**
     * Singleton instance.
     *
     * @param array $options A key=>value array of driver-specific connection options.
     */
    private function __construct(string $dsn, string $username, string $passwd, array $options)
    {
        try {
            parent::__construct($dsn, $username, $passwd, $options);
            $this->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);  
        } catch (\PDOException $e) {
            Main::dieIfFail(true, 'Database connection error :(', $e);
        }
    }


    /**
     * Adiciona um valor a uma variável, que será utilizada na consulta.
     *
     * @param string $name  Nome da variável.
     * @param string $value Valor da variável.
     *
     * @return self;
     */
    public function bind(string $name, string $value): self
    {
        $this->stmt->bindValue($name, $value);

        return $this;
    }


    /**
     * Prevents object clonning.
     *
     * @return void.
     */
    protected function __clone()
    {
    }


    /**
     * Commits actual transaction.
     *
     * @return self;
     */
    public function commit(): self
    {
        if ($this->hasTransaction === true) {
            $this->hasTransaction = false;
            $this->commit();
        }

        return self;
    }


    /**
     * Alias for Database:u(), for delete operations.
     *
     * @param string  $sql    SQL insert statement.
     * @param array   $fields Bind parameters.
     * @param boolean $debug  Prints SQL insert and debug informations.
     *
     * @return integer The number of affected rows.
     */
    public static function d(string $sql, array $fields = [], bool $debug = true): int
    {
        return self::u($sql, $fields, $debug);
    }

    /**
     * Query a prepared sql.
     *
     * @return self.
     */
    public function execute(): self
    {
        $this->stmt->execute();

        return $this;
    }


    /**
     * Returns all rows.
     *
     * @return array.
     */
    public function getAll(): array
    {
        return $this->stmt->fetchAll(\PDO::FETCH_ASSOC);
    }


    /**
     * Returns the last inserted id.
     *
     * @return integer.
     */
    public function getInsertId(): int
    {
        return $this->lastInsertId();
    }


    /**
     * Returns the only one allowed instance.
     *
     * @return Database.
     */
    public static function getInstance(): object
    {
        $host     = DB_HOST;
        $username = DB_USER;
        $passwd   = DB_PASS;
        $dbname   = DB_NAME;
        $charset  = defined('DB_CHARSET') ? DB_CHARSET : 'utf8';
        $dsn      = "mysql:host=$host;dbname=$dbname;port=3306;charset=$charset";
        $options  = [
            \PDO::ATTR_ERRMODE           => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_EMULATE_PREPARES  => false,
            \PDO::ATTR_STRINGIFY_FETCHES => false,
        ];

        if (isset(self::$instance) === false) {
            self::$instance = new self($dsn, $username, $passwd, $options);
        }

        return self::$instance;
    }


    /**
     * Returns PDO's statement.
     *
     * @return \PDOStatement.
     */
    public function getStatement(): object
    {
        return $this->stmt;
    }


    /**
     * Returns total results.
     *
     * @return integer;
     */
    public function getTotal(): int
    {
        return $this->stmt->rowCount();
    }


    /**
     * Abstract helper for inserts.
     *
     * @param string  $sql    SQL insert.
     * @param array   $fields Binding fields.
     * @param boolean $debug  Prints additional information.
     *
     * @return integer Returns the last inserted id.
     */
    public static function i(string $sql, array $fields = [], bool $debug = true): int
    {
        $db = self::getInstance();

        try {
            $db->prepare($sql);

            foreach ($fields as $name => $value) {
                $db->bind(":$name", $value);
            }

            $db->execute();
        } catch (\PDOException $e) {
            if (defined('DEBUG') === true || $debug === true) {
                $error = $e->getMessage();
            } else {
                $error = 'Sorry, cannot execute insert sql at the moment.';
            }

            Main::dieIfFail(true, $error, $e);
        }

        return $db->getInsertId();
    }


    /**
     * Returns a prepared SQL.
     *
     * @param string $query  Actual SQL.
     * @param array $options General options.
     *
     * @return \PDOStatement.
     */
    public function prepare(string $sql, array $options = []): \PDOStatement
    {
        $this->stmt = parent::prepare($sql);

        return $this->stmt;
    }


    /**
     * Abstraction helper for quick queries.
     *
     * @param string  $sql    SQL query.
     * @param array   $fields Bind params.
     * @param integer $index  Row index.
     * @param integer $column Column index.
     * @param boolean $debug  Prints the SQL and debug informations.
     *
     * @return array|string  String if both $index and $column are given. Array, otherwise.
     */
    public static function q(string $sql, array $fields = [], int $index = null, string $column = null, bool $debug = false): array|string
    {
        $config = [];
        $file   = ROOT . '/sql/' . str_replace('.', '/', $sql) . '.sql';
        // Verifica se $sql é nome de arquivo ou a própria consuluta.
        if (Util::regExp($sql, '[a-zA-Z0-9_\-\.]+') === $sql && file_exists($file) === true) {
            $sql  = explode('.', $sql);
            $base = $sql[0];
            array_shift($sql);

            ob_start();
            include $file;
            $sql = ob_get_clean();

            Main::dieIfFail(empty($sql) === true, 'SQL file empty.' . (defined('DEBUG') ? " File: $file" : ''));
        }

        if (strpos($sql, '#config:') !== false) {
            preg_match('/#config:(.*)/', $sql, $config);
            Main::dieIfFail(empty($config[1]), 'Model "#config:" está vazio.');
            $config = Util::parseProperties(str_replace(' ', '', $config[1]));
        }

        $db = self::getInstance();
        try {
            foreach ($fields as $name => $value) {
                if ($name === 'required-fields') {
                    foreach ($value as $bindName => $bindValue) {
                        $crrVar = str_replace('.', '_', $bindName);
                        if (gettype($bindValue) === 'array') {
                            $ids = array_map(
                                function ($item) use ($bindName) {
                                    return $bindName . '_' . $item;
                                },
                                $bindValue
                            );
                            $str = "WHERE\n\t$bindName IN (:".join(', :', $ids).")\n";
                        } else {
                            $str = "WHERE\n\t$bindName = :$crrVar\n";
                        }

                        if (strpos($sql, 'WHERE') !== false) {
                            $sql = str_replace('WHERE', "$str\tAND ", $sql);
                        } else if (strpos($sql, 'HAVING') !== false) {
                            $sql = str_replace('HAVING', $str.'HAVING', $sql);
                        } else if (strpos($sql, 'GROUP BY') !== false) {
                            $sql = str_replace('GROUP BY', $str.'GROUP BY', $sql);
                        } else if (strpos($sql, 'ORDER BY') !== false) {
                            $sql = str_replace('ORDER BY', $str.'ORDER BY', $sql);
                        } else if (strpos($sql, 'LIMIT') !== false) {
                            $sql = str_replace('LIMIT', $str.'LIMIT', $sql);
                        } else {
                            $sql .= $str;
                        }

                        $fields[$bindName] = $bindValue;
                    }
                } elseif ($name === 'add-field') {
                    $sqlSubs  = substr($sql, 0, (strpos(strtoupper($sql), 'FROM')));
                    $sql      = substr($sql, strlen($sqlSubs));
                    $sqlSubs .= ', '.$value;
                    $sql      = $sqlSubs."\n\t$sql";
                } elseif ($name === 'add-join') {
                    $pos    = (strpos($sql, "\nWHERE"));
                    $start  = substr($sql, 0, $pos);
                    $end    = substr($sql, $pos);
                    $sql    = "$start\n\t$value\n$end";
                } elseif ($name === 'add-limit') {
                    if (gettype($value) === 'array') {
                        $start = $value[0];
                        $end   = $value[1];
                    } else {
                        $start = 0;
                        $end   = $value;
                    }

                    if (strpos($sql, 'LIMIT') !== false) {
                        preg_match('/LIMIT(\s*)(\d+)([\,\s]+)?(\d+)?/', $sql, $result);
                        $result[3] = ($result[3] ?? '');
                        $result[4] = ($result[4] ?? '');

                        $sql = str_replace("LIMIT{$result[1]}{$result[2]}{$result[3]}{$result[4]}", "LIMIT{$result[1]}$start, $end", $sql);
                    } else {
                        $sql .= "LIMIT $start, $end";
                    }
                } elseif ($name === 'add-order-by') {
                    preg_match('/ORDER BY(\s*)(.*)/', $sql, $result);
                    if (empty($result[1]) === false) {
                        $sql = str_replace($result[0], "ORDER BY\n\t$value", $sql);
                    } else {
                        // Let's find the best place to insert "order" clause.
                        // TODO: check if LIMIT is the first word in line.
                        if (strpos($sql, 'LIMIT') !== false) {
                            $sql = str_replace('LIMIT', "ORDER BY\n\t$value\nLIMIT", $sql);
                        } else {
                            $sql .= "ORDER BY $value";
                        }
                    }
                }
            }

            $db->prepare($sql);

            $alloweds   = explode(',', 'string,integer,double,float,array');
            $forbiddens = explode(',', 'add-field,add-join,add-limit,add-order-by,required-fields,add-where,add-group');
            foreach ($fields as $name => $value) {
                $crrVar = str_replace('.', '_', $name);
                if (in_array(gettype($value), $alloweds) === true && in_array($name, $forbiddens) === false) {
                    if (gettype($value) === 'array') {
                        foreach ($value as $crrValue) {
                            $stmt->bind(":{$crrVar}_$crrValue", $crrValue);
                            if ($debug === true) {
                                echo "[:{$crrVar}_$crrValue => $crrValue]\n";
                            }
                        }
                    } else {
                        $db->bind(":$crrVar", $value);
                        if ($debug === true) {
                            echo "[:$crrVar => $value]\n";
                        }
                    }
                }
            }
    
            $db->execute();
        } catch (\Exception $e) {
            $additional = '';
            if ($debug === true || defined('DEBUG') === true) {
                if ($file === null) {
                    $additional = "File used: \"$file\".<br>";
                } else {
                    $additional = "File used: n/a<br>SQL: \"$sql\"." . $e->getMessage();
                }

                $additional .= "Fields:\n";
                foreach ($fields as $field => $value) {
                    if (gettype($value) === 'array') {
                        $value = print_r($value, true);
                    }

                    $additional  .= "\t$field=>$value\n";
                }
            }

            Main::dieIfFail(true, "Cannot execute sql at the moment. $additional", $e);
            return [];
        }

        $rows = $db->getAll(self::FETCH_ASSOC);
        if (empty($config['json']) === false) {
            $cols = explode(',', ($fields['json'] ?? $config['json']));
            foreach ($rows AS &$row) {
                foreach ($cols AS $col) {
                    $row[$col] = json_decode($row[$col], true);
                }
            }
        }

        if (empty($config['time-ellapsed']) === false) {
            $cols = explode(',', ($fields['time-ellapsed'] ?? $config['time-ellapsed']));
            foreach ($rows AS &$row) {
                foreach ($cols AS $col) {
                    $row[$col] = \Mock\Core\Util::secondsToEllapsed($row[$col]);
                }
            }
        }

        if (empty($config['bytes']) === false) {
            $cols = explode(',', ($fields['bytes'] ?? $config['bytes']));
            foreach ($rows AS &$row) {
                foreach ($cols AS $col) {
                    $row[$col] = Util::integerToBytes((int) $row[$col]);
                }
            }
        }

        if ($index !== null) {
            if ($column !== null) {
                return ($rows[$index][$column] ?? '');
            } else {
                return ($rows[$index] ?? []);
            }
        }

        return $rows;
    }
    
    
    /**
     * Rollback currenct database transaction.
     *
     * @return self;
     */
    public function rollback(): self
    {
        if ($this->hasTransaction === true) {
            $this->hasTransaction = false;
            $this->driver->rollback();
        }

        return $this;
    }


    /**
     * Starts a new ACID transaction.
     *
     * @return self.
     */
    public function start(): self
    {
        if ($this->hasTransaction === false) {
            $this->hasTransaction = true;
            $this->beginTransaction();
        }

        return self;
    }


    /**
     * Helper for SQL operation update/delete.
     *
     * @param string  $sql    SQL insert statement.
     * @param array   $fields Bind parameters.
     * @param boolean $debug  Prints SQL insert and debug informations.
     *
     * @return integer The number of affected rows.
     */
    public static function u(string $sql, array $fields = [], bool $debug = false): int
    {
        $db = self::getInstance();

        try {
            $stmt = $db->prepare($sql);

            foreach ($fields as $name => $value) {
                $stmt->bindValue(":$name", $value);
            }

            $stmt->execute();
        } catch (\PDOException $e) {
            $error = 'Sorry, cannot execute change the registry at the moment.';
            if ($debug === true || defined('DEBUG') === true) {
                $error .= " SQL:\n$sql\nFields:\n";
                foreach ($fields as $name => $value) {
                    $error .= ":$name = $value\n";
                }
            }

            Main::dieIfFail(true, $error, $e);
        }

        return $db->stmt->rowCount();
    }
    
    
}