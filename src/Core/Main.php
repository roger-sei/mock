<?php

namespace Mock\Core;


class Main
{

    public $data = [];


    /**
     * Assert helper. Dies and exit if $assert is false.
     *
     * @param boolean    $assert   Assert condition.
     * @param string     $message  Error message.
     * @param \Exception $e        Exception object.
     * @param string     $status   HTTP Status code. Default: 503 System Error.
     * @param string     $mimeType Mime type for content-type response.
     *
     * @return void.
     */
    public static function dieIfFail(bool $assert, string $message, \Exception $e = null, string $status = null, string $mimeType = null): void
    {
        if ($assert === true) {
            if (defined('DEBUG') === true && $e !== null) {
                $message .= PHP_EOL . 'Additional info: '. $e->getMessage();
            }

            self::finish(
                [
                    'status'  => $status,
                    'error'   => $message,
                ],
                $status,
                $mimeType
            );
        }
    }


    /**
     * Prints and exits the application.
     *
     * @param array  $results  Data content for response.
     * @param string $status   HTTP Status code. Default: "200 System Error".
     * @param string $mimeType Mime type for content-type response.
     *
     * @return void.
     */
    public static function finish(array $results, string $status = null, string $mimeType = null): void
    {
        if (empty($results['success']) === false) {
            $type = 'success';
        } elseif (empty($results['error']) === false) {
            $type   = 'error';
            $page   = 'error';
            $status = ($status ?? '503 Generic system error');
        } elseif (empty($results['warning']) === false) {
            $type   = 'warning';
            $status = ($status ?? '200 Ok With Warnings');
        } elseif (empty($results['message']) === false) {
            $type = 'message';
        } elseif (empty($results['fatal']) === false) {
            $type   = 'fatal';
            $page   = 'error';
            $status = ($results['status'] ?? '503 System error');
        } else {
            throw new \Exception('Unkown $results type.');
        }

        $mimeType = ($mimeType ?? self::getMimeType());

        header("HTTP/1.1 $status");
        if ($mimeType === 'application/json') {
            header('Content-type: application/json');
            echo json_encode($results);
        } elseif ($mimeType === 'application/xml') {
            header('Content-type: application/xml');
            echo '<?xml version="1.0" encoding="UTF-8"?><' . $type . '>' . $results['type'] . '</' . $results['type'] . '>';
        } else {
            header('Content-type: text/html; charset=utf-8');
            if ($type === 'error' || $type === 'fatal') {
                $page = 'error';
            } else {
                $page = 'message';
            }

            if ($type === 'success' && empty($_REQUEST['redirect']) === false) {
                header("Location: {$_REQUEST['redirect']}");
            } elseif (file_exists(ROOT . "/Pages/$page.inc") === true) {
                $instance = new self();
                $instance->data = $results;
                $instance->print(ROOT . "/Pages/$page.inc");
            } else {
                echo $results[$type];
            }
        }

        exit;
    }


    /**
     * Returns the internal HTTP Rest format.
     *
     * @return string.
     */
    public static function getMimeType(): string
    {
        $headers  = getallheaders();
        $mimeType = 'text/json';
        foreach ($headers AS $name => $value) {
            if (strtolower(trim($name)) === 'content-type') {
                $mimeType = strtolower(trim($value));
                break;
            }
        }

        return $mimeType;
    }


    /**
     * Checks if a given name is a valid log.
     *
     * @param string  $path Path of log, as saved in config.
     * @param boolean $cond Assert condition. If true, a 404 page will be displayed.
     *
     * @return void.
     */
    public function on404(bool $cond, string $path = ''): void
    {
        // TODO: create a view 404.inc.
        if ($cond === true) {
            $path = Util::regExp($path, '[a-zA-Z0-9\-_\.]+');
            die("Page $path does not exists");
        }
    }


    /**
     * Parse and load files accordingly to the configured route.
     *
     * @param string $page URL page.
     *
     * @return void.
     */
    protected function parseRoutes(string $page): void
    {
        $view       = null;
        $controller = null;
        $args       = [];
        $routes     = [];
        $path       = $page;
        while (true) {
            if (str_ends_with($path, '/') === false) {
                $path = "$path/";
            }

            // First, check for file on disk.
            $test = substr(preg_replace_callback(
                '/([\/\-][a-z])/',
                fn($matches) => strtoupper(str_replace('-', '', $matches[1])),
                "/$path"
            ), 1);

            if (str_ends_with($test, '/') === true) {
                $test = substr($test, 0, -1);
            }

            if (file_exists(ROOT . "/Pages/$test.php") === true || file_exists(ROOT . "/$test.php") === true) {
                if (str_starts_with($test, 'Services/') === false) {
                    $test = "Mock/Pages/$test";
                }

                $controller = str_replace('/', '\\', $test);
                break;
            }

            $path = substr($path, 0, -1);
            if (strpos($path, '/') === false) {
                break;
            } else {
                $path = substr($path, 0, strrpos($path, '/'));
            }
        }

        if (empty($controller) === false) {
            $pageNorm = $page;
            if (str_ends_with($pageNorm, '/') === false) {
                $pageNorm = "$pageNorm/";
            }

            $args = explode('/', substr($page, strlen($path)));
            array_pop($args);
        }

        // If not found, check for defined routes.
        $path = $page;
        if (str_ends_with($path, '/') === false) {
            $path = "$path/";
        }

        if (empty($controller) === true) {
            preg_match_all('/([a-zA-Z0-9\\\\_]+)\s*\=\s*(\/.*\/)/', file_get_contents(ROOT . '/config/routes.ini'), $matches);
            for ($i = 0, $length = count($matches[0]); $i < $length; $i++) {
                $routes[$matches[1][$i]] = $matches[2][$i];
            }

            // Check if it's a route.
            foreach ($routes as $dest => $regex) {
                if (preg_match($regex, $path, $matches) === 1) {
                    $controller = $dest;
                    $args       = $matches;
                    array_shift($args);
                    break;
                }
            }
        }

        $this->dieIfFail(empty($controller), 'Page not found!', null, 404);
        $reflect = new \ReflectionClass($controller);
        if ($reflect->getConstructor() === null) {
            $instance = $reflect->newInstance();
        } else {
            $instance = $reflect->newInstanceArgs($args);
        }

        if (str_starts_with($controller, 'Services\\') === true) {
            $instance->run();
        }

        if (str_starts_with($controller, 'Mock\\')) {
            $view = preg_replace_callback(
                '/([A-Z]+)/',
                fn($matches) => strtolower("-$matches[1]"),
                Util::regExp($controller, '\\\([0-9a-zA-Z_\-\.]+)$')
            );
            $view = Util::regExp($controller, '^Mock\\\(.*)\\\\') . "\\$view.inc";
            $view = str_replace('\\', '/', $view);
            $view = ROOT . '/' . str_replace('/-', '/', $view);
        }

        if ($view !== null && file_exists($view) === true) {
            $instance->print($view);
        }
    }


    /**
     * Prints a view and shares a data object.
     *
     * @param string $file Path of file.
     *
     * @return void.
     */
    public function print(string $file): void
    {
        header('Content-type: text/html; charset=utf-8');
        include $file;
    }


}
