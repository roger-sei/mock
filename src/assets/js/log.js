function CheckSize() {
    window.crrReq = Mock.Request(`/services/logs/last-id/?id-config=${window.idConfig}&id-log=${window.lastInsertId}`).then(json => {
        if (window.paused === true) {
            return;
        }

        if (json.lastInsertId !== undefined) {
            if (json.lastInsertId > window.lastInsertId) {
                window.scrollTo(0, 0);
                document.body.style.overflow = 'hidden';
                location.reload();
            } else {
                window.crrTimeout = setTimeout(CheckSize, 1000);
            }
        } else {
            window.crrTimeout = setTimeout(CheckSize, 10000);
        }
    });
}

Actions['log-pause'] = () => {
    window.paused = !window.paused;
    let eImg      = Mock(`.bt-pause img`),
        options   = eImg.getAttribute('data-src').parseProperties();

    if (window.paused === true) {
        eImg.src = options.resume;
        clearTimeout(window.crrTimeout);
        crrReq.abort();
    } else {
        eImg.src = options.pause;
        CheckSize();
    }
};
Actions['log-erase'] = () => {
    Mock.Request({
        url:            `/services/logs/?id-config=${window.idConfig}`,
            method:     'delete',
            onComplete: response => {
                location.reload();
            }
    });
};
Actions['log-collapse::toggle'] = function() {
    document.body.classList.toggle('collapsed');
    let status = this.classList.toggle('inactive');
    Mock.Cookie.set('log-collapse', (status === true ? '' : 'collapsed' ));
}
Queue.push(() => {
    window.crrTimeout = CheckSize();
});

function ShowLogPreview(options) {
    if (typeof options.eDestCard.__content !== 'undefined') {
        return;
    }

    let contents = this.getParent('section').q('.log__response').textContent;

    options.eDestCard.__content = contents;
    let mimeType = options.eDestCard.getAttribute('data-mime-type').toLowerCase();

    if (mimeType === 'application/json') {
        options.eDestCard.innerHTML = ParseJSON(contents);
    } else if (mimeType === 'application/xml') {
        options.eDestCard.innerText = ParseXML(contents);
    } else {
        options.eDestCard.innerHTML = contents;
    }
    
}
function ParseJSON(contents) {
    try {
        let json = JSON.parse(contents);

        contents = JSON.stringify(json, null, 4);
        contents = contents.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
        contents = contents.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
            var cls = 'number';
            if (/^"/.test(match)) {
                if (/:$/.test(match)) {
                    cls = 'key';
                } else {
                    cls = 'string';
                }
            } else if (/true|false/.test(match)) {
                cls = 'boolean';
            } else if (/null/.test(match)) {
                cls = 'null';
            }
            return '<span class="' + cls + '">' + match + '</span>';
        });
        contents = `<pre>${contents}</pre>`;
    } catch (e) {
        throw new Error(e);
    }

    return contents;
}
function ParseXML(contents) {
    var xmlDoc = new DOMParser().parseFromString(contents, 'application/xml');
    var xsltDoc = new DOMParser().parseFromString([
        // describes how we want to modify the XML - indent everything
        '<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform">',
        '  <xsl:strip-space elements="*"/>',
        '  <xsl:template match="para[content-style][not(text())]">', // change to just text() to strip space in text nodes
        '    <xsl:value-of select="normalize-space(.)"/>',
        '  </xsl:template>',
        '  <xsl:template match="node()|@*">',
        '    <xsl:copy><xsl:apply-templates select="node()|@*"/></xsl:copy>',
        '  </xsl:template>',
        '  <xsl:output indent="yes"/>',
        '</xsl:stylesheet>',
    ].join('\n'), 'application/xml');

    var xsltProcessor = new XSLTProcessor();    
    xsltProcessor.importStylesheet(xsltDoc);
    var resultDoc = xsltProcessor.transformToDocument(xmlDoc);
    var resultXml = new XMLSerializer().serializeToString(resultDoc);
    return resultXml;
}

window.addEventListener('load', () => {
    document.querySelector('#eLogs').addEventListener('click', function(e){
        let obj = e.target;
        if (obj === null) {
            return;
        }

        let eCard = obj.closest('.card-tab');
	if ( eCard === null) {
            return;
        }

        eCard.classList.add('active');
        eCard.scrollIntoView({
            behavior: 'smooth',
            block:     'end',
	});
    });
});
