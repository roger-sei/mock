document.body.addEventListener('click', function(e, customOptions) {
    var eA = e.target, i;
    customOptions = customOptions || {};
    while (eA !== null && eA !== document.body) {
        if (eA.getAttribute('data-actions') !== null) {
            var type,
                options = eA.getAttribute('data-actions').parseProperties();
            for (i in options) {
                if (typeof i === 'string') {
                    var subOptions = {};
                    if (typeof Actions[i] === 'function') {
                        if (typeof options[i] === 'string') {
                            subOptions = options[i].parseProperties(',');
                        }

                        Actions[i].call(eA, e, subOptions);
                    }
                }
            }
        }

        eA = eA.parentNode;
    }
});