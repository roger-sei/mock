/**
 * @update 2021/02/21 Using Vanila js.
 */
Queue.push(root => {
	Mock.qq('[data-auto-update]').forEach(eInput => {
		let data = {};
		eInput.addEventListener('change', function() {
			this.dispatchEvent(new Event('DataAutoUpdate'));
		});
		eInput.addEventListener('keydown', function(e) {
			if (e.keyCode === 13 && e.shiftKey === false && e.altKey === false && this.nodeName !== 'TEXTAREA') {
				this.trigger('DataAutoUpdate', e);
				e.stop();
			}
		});
		eInput.addEventListener('saveBackup', function(e) {
			if (this.type === 'checkbox') {
				data['value.bkp'] = this.checked;
			} else if (this.nodeName === 'SELECT') {
				data['value.bkp'] = this.selectedIndex;
			} else {
				data['value.bkp'] = this.value;
			}
		});
		eInput.addEventListener('rollback', function(e) {
			if (this.type === 'checkbox') {
				let eLabel = this.getParent('.checkbox');
				this.checked = data['value.bkp'];
				if (eLabel !== null) {
					if (this.checked === true) {
						eLabel.classList.add('checked');
					} else {
						eLabel.classList.remove('checked');
					}
				}
			} else if (this.nodeName === 'SELECT') {
				this.selectedIndex = data['value.bkp'];
			} else {
				this.value = data['value.bkp'];
			}

			this.classList.remove('loading');
			var eForm = this.getParent('form');
			eForm.classList.remove('loading');
		});
		
		eInput.dispatchEvent(new Event('saveBackup'));

		eInput.addEventListener('DataAutoUpdate', function(e) {
			if (this.classList.contains('loading') === true) {
				return false;
			} else {
				// Verifica se há alterações.
				if (this.type === 'checkbox' || this.type === 'radio') {
					if (this.checked === data['value.bkp']) {
						return false;
					}
				} else if (this.nodeName === 'SELECT') {
					if (this.selectedIndex === data['value.bkp']) {
						return false;
					}
				} else {
					if (this.value === data['value.bkp']) {
						return false;
					}
				}
			}

			this.classList.add('loading');

			var value   = this.value;
			var eInput  = this;
			var eForm   = this.getParent('form');
			var options = (this.getAttribute('data-auto-update') || '').parseProperties();

			options.action = (options.action || 'update');
			options.method = (options.method || eForm.getAttribute('method') || 'patch');
			options.fields = (options.fields || {});

			if (typeof options.fields === 'string') {
				options.fields = options.fields.parseProperties(',');
			}

			var gets  = {},
				posts = {}
			;

			eForm.classList.add('loading');
			options.url = eForm.action;

			for (var i = 0, eHiddens = eForm.qq('input[type="hidden"]'), length = eHiddens.length; i < length; i++) {
				if (options.method === 'get') {
					gets[eHiddens[i].name] = eHiddens[i].value;
				} else {
					posts[eHiddens[i].name] = eHiddens[i].value;
				}
			}

			if (this.type === 'checkbox') {
				value = this.checked ? (this.value || 1) : 0;
				if (typeof options.sendAsListFrom === 'string') {
					value = [];
					var eParent = this.getParent(options.sendAsListFrom) || Mock(options.sendAsListFrom);
					eParent.qq('input[name="'+ this.name +'"]').forEach(item => {
						if (item.checked === true) {
							value.push(item.value);
						}
					});
					value = value.join(',');
				}
			} else if (this.type === 'radio') {
				value = this.value;
			} else if (this.nodeName === 'SELECT') {
				value = this.value;
			}

			if (options.method === 'get') {
				gets.field = this.name;
				if (value.length > 3900) {
					gets.value = value;
				} else {
					gets.value = value;
				}
			} else {
				posts.field = this.name;
				posts.value = value;
			}

			if (typeof options.id !== 'undefined') {
				if (options.method === 'post') {
					posts.id = options.id;
				} else {
					gets.id = options.id;
				}
			}

			for (i in options.fields) {
				if (typeof options.fields[i] === 'string' ||
					typeof options.fields[i] === 'number' ||
					typeof options.fields[i] === 'boolean'
				) {
					if (options.method === 'post') {
						posts[i] = options.fields[i];
					} else if (typeof options.fields[i] === 'string') {
						if (options.method.toLowerCase() === 'get') {
							gets[i] = options.fields[i];
						} else {
							posts[i] = options.fields[i];
						}
					}
				}
			}

			var Request = function() {
				Mock.Request({
					url:        options.url,
					method:     options.method,
					posts:      posts,
					gets:       gets,
					onComplete: json => {
						eForm.json = json;
						eInput.classList.remove('loading');
						eForm.classList.remove('loading');

						var type    = 'message',
							message = json.success || json.error || json.message;

						if (json.success) {
							type = 'success';
							eForm.dispatchEvent(new Event('onActionSuccess'), json);
						} else {
							type = 'error';
							eForm.dispatchEvent(new Event('onActionError'));
						}

						if (type === 'error') {
							if (json.field !== undefined) {
								let eField  = eForm.q(`[name="${json.field}"]`),
									eParent = eField && eField.getParent('label'),
									eError  = eParent.q('.error');
								if (eError !== null) {
									eError.classList.add('show');
									eError.innerHTML = json.error;
									try {
										eField.focus();
										eField.select();
									} catch(e) {}
								}
							} else if (options.errorElement !== undefined) {
								let selector = typeof options.errorElement === 'string' ? options.errorElement : '.results',
									eError   = eForm.q(selector);
								eError.innerHTML = message;
								eError.classList.add('show');
							} else if (options.showDialog !== undefined) {
								Mock.whenReady([[Mock, 'showDialog', 'function']]).then(function(){
									Mock.showDialog({
										type:    type,
										title:   json.title || '',
										message: message,
									});
								});
							}

							eInput.dispatchEvent(new Event('rollback'));
						} else {
							if (options.redirect === 'back') {
								location.href = '../';
								location.href = options.redirect;
								return;
							} else if (options.redirect === 'self') {
								location.href = './';
								return;
							} else if (options.redirect !== undefined) {
								location.href = options.redirect;
								return;
							}

							eInput.dispatchEvent(new Event('saveBackup'));
						}

						if (options.showNotification === undefined  || (type === 'error' && options.showNotification.indexOf('errors-only') > -1)) {
							Mock.load('bin/showNotification.js');
							Mock.whenReady([[Mock, 'showNotification', 'function']]).then(function(){
								Mock.showNotification({
									type:     type,
									title:    json.title || '',
									message:  message,
								});
							});
						}

						eForm.dispatchEvent(new Event('onActionComplete'));
					},
					onError:    function() {
						eInput.removeClass('loading');
						eForm.classList.remove('loading');

						//eInput.setAttribute('pattern','[1]{30,30}');
						eInput.trigger('rollback');
						
						Mock.load('bin/showDialog.js');
						Mock.whenReady([[Mock, 'showDialog', 'function']], function() {
							Mock.showDialog({
								message: 'Server error. Please, wait a few decades and try again.',
							});
						});
					},
				});
			};

			if (options.confirm !== undefined) {
				var type = options.type || 'confirm';

				Mock.load('bin/showDialog.js');
				Mock.whenReady([[Mock, 'showDialog', 'function']]).then(function(){
					Mock.showDialog({
						title:     options.title || '',
						type:      type,
						message:   options.message || options.confirm,
						onConfirm: function() {
							this.close();
							Request();
						},
						onCancel:  function() {
							this.close();
							eInput.dispatchEvent(new Evebt('rollback'));
						},
					});
				});
			} else {
				Request();
			}
		});
		eInput.addEventListener('keyup', function() {
			if (this.getAttribute('pattern') === '[1]{30,30}') {
				this.removeAttribute('pattern');
			}
		});

		if (this.nodeName === 'BUTTON') {
			this.addEventListener('click', function(e) {
				e.stop();
				this.dispatchEvent(new Event('change'));
			});
		}
	});
});