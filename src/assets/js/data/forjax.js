/**
 * option title           Título para o dialogBox.
 * option confirm         Se provido, um dialogBox com uma mensagem de confirmação será exibida.
 * option message
 * option errorElement    Indica um elemento, em formato css selector, para receber a mensagem de erro, caso haja algum.
 * option field           Similar ao errorElement, mas em caso de erro, é buscado o element .error do field.
 *                        Exemplo: <label><span>CPF</span><input name="cpf" ><small class="error" ></small></label></label>
 * option useOverlay
 * option useDialog       Options: error-only
 * option suppressError
 * option suppressSuccess
 * option onSuccess       Possible Values: reload, back
 *
 * @listener onActionComplete(e, json)
 * @listener onActionError
 * @listener onActionSuccess
 *
 * @update 2019/03/31 
 */
Queue.push(root => {
	root.qq('form[data-forjax]').forEach( eForm => {
		eForm.addEventListener('submit', e => {
			e.preventDefault();
			eForm.dispatchEvent(new Event('Submit'));
		});
		eForm.addEventListener('Submit', e => {
			var options = (eForm.getAttribute('data-forjax') || '').parseProperties();
			var method  = eForm.method || 'get';

			var ignoreOnEmpty = false;
			var gets          = {};
			var posts         = {};
			var eInputs       = eForm.qq('input, select, textarea');
			eInputs.forEach( eItem => {
				if (eItem.type === 'radio') {
					let name = eItem.name;
					if (eItem.checked === true) {
						if (method === 'post') {
							posts[name] = eItem.value;
						} else {
							gets[name] = eItem.value;
						}
					}
				} else if (eItem.name.length > 0 && eItem.getAttribute('data-forjax-ignore') === null) {
					if (method === 'post') {
						posts[eItem.name] = eItem.value;
					} else {
						gets[eItem.name] = eItem.value;
					}
				}

				if (eItem.getAttribute('data-ignore-min-length') !== null && eItem.value.length < eItem.getAttribute('data-ignore-min-length')) {
					ignoreOnEmpty = true;
				}
			});

			var startRequest = function(){
				Mock.Request({
					url:     eForm.action,
					method:  method,
					gets:    gets,
					posts:   posts,
					onError: function() {
						eForm.classList.remove('loading');
						if (options.suppressError === undefined) {
							Mock.showDialog({
								type: 'error',
								message: 'Woops, system error. <br />Please, wait a few minutes and try again.',
								onClose: function() {
									eForm.dispatchEvent(new Event('onDialogClose'));
								}
							});
						}

						eForm.dispatchEvent(new Event('onActionComplete'));
						eForm.dispatchEvent(new Event('onActionError'));
					},
					onComplete: json => {
						eForm.classList.remove('loading');
						eForm.json = json;

						var type    = 'message',
							message = json.success || json.error || json.message;

						if (json.success) {
							type = 'success';
							eForm.dispatchEvent(new Event('onActionSuccess'), json);
						} else {
							type = 'error';
							eForm.dispatchEvent(new Event('onActionError'));
						}

						if (type === 'error') {
							if (json.field !== undefined) {
								let eField  = eForm.q(`[name="${json.field}"]`),
									eParent = eField && eField.getParent('label'),
									eError  = eParent.q('.error');
									if (eError !== null) {
										eError.classList.add('show');
										eError.innerHTML = json.error;
										try {
											eField.focus();
											eField.select();
										} catch(e) {}
									}
								;
							} else if (options.errorElement !== undefined) {
								let selector = typeof options.errorElement === 'string' ? options.errorElement : '.results',
									eError   = eForm.q(selector);
								eError.innerHTML = message;
								eError.classList.add('show');
							} else if (options.showNotification !== undefined) {
								Mock.whenReady([[Mock, 'showNotification', 'function']]).then(function(){
									Mock.showNotification({
										type:    type,
										title:   json.title || '',
										message: message,
									});
								});
							} else if (options.showDialog !== undefined) {
								Mock.whenReady([[Mock, 'showDialog', 'function']]).then(function(){
									Mock.showDialog({
										type:    type,
										title:   json.title || '',
										message: message,
									});
								});
							}
						} else {
							if (options.redirect === 'back') {
								location.href = '../';
								location.href = options.redirect;
								return;
							} else if (options.redirect === 'self') {
								location.href = './';
								return;
							} else if (options.redirect !== undefined) {
								location.href = options.redirect;
								return;
							}
						}
						eForm.dispatchEvent(new Event('onActionComplete'));
					}
				});

				// Spinner...
				eForm.classList.add('loading');
			};

			if (options.showNotification !== undefined) {
				Mock.loadScript('bin/showNotification.js');
			}

			if (options.confirm !== undefined) {
				var type = options.type || 'confirm';
				Mock.showDialog({
					type:      type,
					data:      options,
					message:   type,
					onConfirm: function() {
						startRequest();
					},
				});
			} else {
				startRequest();
			}
		});
	});
});