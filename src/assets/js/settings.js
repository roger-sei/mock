Actions['erase-rule'] = function(e, options) {
    let eSection = this.getParent('section');

    Mock.load('bin/showNotification.js');

    Mock.Request({
        url:    '/services/configs/rules/',
        method: 'DELETE',
        posts:  {
            id: options.id,
        },
    }).then(json => {
        if (json.success) {
            Mock.whenReady([[Mock, 'showNotification', 'function']]).then(() => {
                Mock.showNotification({
                    type:    'success',
                    title:   'Success',
                    message: json.success,
                });
            });

            eSection.classList.add('remove');
            setTimeout(() => {
                eSection.remove();
            }, 800);
        } else {
            Mock.whenReady([[Mock, 'showNotification', 'function']]).then(() => {
                Mock.showNotification({
                    type:    'error',
                    title:   'Error',
                    message: json.error,
                });
            });
        }
    });
};

Queue.push(() => {
    
});