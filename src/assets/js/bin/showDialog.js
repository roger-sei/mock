"use strict";
/**
 * Substitui a caixa de alerta para exibir notificações.
 * Nota: embora seja um método final, autoinstanciado, é possível abrir nova instância usando o option createNew
 *
 * @option string             title
 * @option string|HTMLElement message
 *
 * @method onCancel(e)
 * @method onClose(e)
 * @method onComplete()
 *
 * @update 2021/02/20 - Code reduced by 25%
 */
var Queue = (Queue || []);
Queue.push(root => {
	function CloseDialog() {
		var eDialogs = Mock.qq('.dialog__container'),
			eDialog  = eDialogs[eDialogs.length - 1];
		if (eDialog instanceof HTMLElement) {
			eDialog.classList.remove('show');
			setTimeout(() => {
				eDialog.remove();
			}, 1000);
		}
	}

	if ('documentElement' in document === true ) {
		document.documentElement.addEventListener('keydown', e => {
			if (['INPUT', 'SELECT', 'TEXTAREA'].indexOf(e.target.nodeName) === -1 && e.keyCode === 27) {
				CloseDialog();
			}
		});
	}

	Mock.showDialog = function(options){
		var template = `
			<div class="dialog__container" >
				<section class="dialog {type}" >
					<h3 class="dialog__title" >
						{title}
					</h3>
					<div class="dialog__message" >
						{message}
					</div>
					<footer class="dialog__footer" >
						<button class="dialog__button--cancel button gray" >{cancel}</button>
						<button class="dialog__button--ok button" value="{ok}" >{ok}</button>
					</footer>
				</section>
				<div class="dialog__overlay" ></div>
			</div>
		`;

		options.type = (options.type || 'success');

		var data = options;
		data.ok     = options.ok || 'OK';
		data.cancel = options.cancel || 'Cancel';

		let reg;
		for (var i in data) {
			if (typeof data[i] === 'string') {
				reg = new RegExp("\{"+i+"\}", 'g');
				template = template.replace(reg, data[i]);
			}
		}

		var eNew = document.createElement('DIV');
		eNew.innerHTML = template;

		var eOverlay = eNew.q('.dialog__overlay'),
			eOk      = eNew.q('.dialog__button--ok'),
			eCancel  = eNew.q('.dialog__button--cancel'),
			eDialog  = eNew.q('.dialog'),
			eTitle   = eNew.q('.dialog__title'),
			eMessage = eNew.q('.dialog__message'),
			eMain    = eNew.q('.dialog__container')
		;

		if (options.message instanceof HTMLElement) {
			eMessage.innerHTML = '';
			eMessage.appendChild(options.message);
		}

		Queue.obstinate(eNew);

		eOverlay.addEventListener('click', CloseDialog);
		eOk.addEventListener('click', CloseDialog);
		eCancel.addEventListener('click', CloseDialog);

		document.body.appendChild(eMain);
		setTimeout(() => {
			eMain.classList.add('show');
		}, 80);

		return eDialog;
	};
	Mock.load('dialog.css');
});