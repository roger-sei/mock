"use strict";
/**
 * Manager a centralized notification system.
 *
 * @option string             title
 * @option string|HTMLElement message
 * @option integer            duration Milliseconds to show off the notificaion. Default 4000 for successfull messages.
 *
 * @method onCancel(e)
 * @method onClose(e)
 * @method onComplete()
 *
 * @update 2021/02/20 - Code forked from showDialog.js
 */
var Queue = (Queue || []);
Queue.push(root => {
	function Close(eNotification) {
		eNotification.classList.add('close');
		setTimeout(() => {
			eNotification.remove();
		}, 500);
	}
	function GetContainer() {
		if (GetContainer.element === undefined) {
			GetContainer.element = document.createElement('DIV');
			GetContainer.element.classList.add('notification__container');
			document.body.appendChild(GetContainer.element);
		}

		return GetContainer.element;
	}

	if ('documentElement' in document === true ) {
		document.documentElement.addEventListener('keydown', e => {
			if (['INPUT', 'SELECT', 'TEXTAREA'].indexOf(e.target.nodeName) === -1 && e.keyCode === 27) {
				CloseDialog();
			}
		});
	}

	Mock.showNotification = function(options){
		var template = `
				<section class="notification {type}" >
					<h3 class="notification__title" >
						{title}
						<a class="notification__close" >x</a>
					</h3>
					<div class="notification__message" >
						{message}
					</div>
				</section>
		`;

		options.type = (options.type || 'success');

		var data = options;
		data.ok     = options.ok || 'OK';
		data.cancel = options.cancel || 'Cancel';

		let reg;
		for (var i in data) {
			if (typeof data[i] === 'string') {
				reg = new RegExp("\{"+i+"\}", 'g');
				template = template.replace(reg, data[i]);
			}
		}

		let eNew = document.createElement('DIV');
		eNew.innerHTML = template;

		let eNotification = eNew.q('.notification'),
			eMessage      = eNew.q('.notification__message'),
			eClose        = eNew.q('.notification__close'),
			eContainer    = GetContainer();

		if (options.message instanceof HTMLElement) {
			eMessage.innerHTML = '';
			eMessage.appendChild(options.message);
		}

		eClose.addEventListener('click', () => { Close(eNotification); });

		eContainer.insertBefore(eNotification, eContainer.firstChild);
		setTimeout(() => {
			eNotification.classList.add('show');
			let duration = options.duration;
			if (typeof duration === 'undefined') {
				if (options.type === 'error') {
					duration = 15000;
				} else if (options.type === 'warning') {
					duration = 10000;
				} else {
					duration = 4000;
				}
			}
			setTimeout(() =>{
				Close(eNotification);
			}, duration);
		}, 80);

		return eNotification;
	};
	Mock.load('notification.css');
});