Queue.push(() => {
   function CheckSize() {
       Mock.Request(`/services/configs/last-id/?id-config=${window.lastInsertId || 0}`).then(json => {
           if (window.paused === true) {
               return;
           }
   
         if (json.lastInsertId > window.lastInsertId) {
             location.reload();
         } else {
             window.crrTimeout = setTimeout(CheckSize, 1000);
         }
       });
   }
   
   CheckSize();
});