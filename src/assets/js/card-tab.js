Queue.push(()=>{
    Actions['card-tab'] = function(e, options) {
        let destIndex     = this.getNodeIndex(),
            eParent       = this.getParent('.card-tab'),
            eTabs         = eParent.querySelectorAll('.card-tab__tab'),
            eCards        = eParent.querySelectorAll('.card-tab__card'),
            ePreviousTab  = (eParent.querySelector('.card-tab__tab.active') || this),
            previousIndex = ePreviousTab.getNodeIndex(),
            ePreviousCard = eCards[previousIndex];
            eDestCard     = eCards[destIndex];
            eDestTab      = eTabs[destIndex];

        if (options.open === true || (options.toggle === true && this.classList.contains('active') === false)) {
            eTabs.forEach(item => {
                item.classList.remove('active');
            });
            eCards.forEach(item => {
                item.classList.remove('active');
            });
            eDestTab.classList.add('active');
            eDestCard.classList.add('active');
        } else if (options.toggle === true) {
            eDestTab.classList.remove('active');
            eDestCard.classList.remove('active');
        }

        if (typeof options.callback === 'string') {
            if (typeof window[options.callback] !== 'function') {
                throw new Error(`Callback "[${options.callback}] not found!`);
            }

            window[options.callback].call(this, {
                destIndex:     destIndex,
                eDestTab:      eDestTab,
                eDestCard:     eDestCard,
                previousIndex: previousIndex,
                ePreviousTab:  ePreviousTab,
                ePreviousCard: ePreviousCard,
                options:       options,
            });
        }
    };
});
