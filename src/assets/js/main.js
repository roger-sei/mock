/**
 * Queue Model for jCube.
 *
 * Note que Queue executa somente uma vez.
 * Para persistência, utilize jCube.init.push(fx) e jCube.init.execute(root)
 * Ou Queue.push([fx, root, true]).
 *
 * @update 2018/08/17
**/
var Queue   = Queue || [];
var Actions = Actions || {};

Queue.views = [];
Queue.execute = function(root) {
    root = (root || document.body);

    var htmlContent = root.innerHTML.toLowerCase(),
        len         = this.length,
        crr         = null;
    for (var i = 0; i < len; i++) {
        crr = this[i];
        if (typeof crr === 'function') {
            crr(root, htmlContent);
        } else {
            crr[0](root, htmlContent);
            Queue.views.push(crr[0]);
        }
    }

    Queue.length = 0;
};
Queue.obstinate = function(root) {
    Queue.execute.call(Queue.views, root);
};
Queue.push = function(fx) {
    Array.prototype.push.call(this, fx);
    this.execute();
};
var Mock = (function() {
    function Mock(selector) {
        return document.querySelector(selector);
    }

    Mock.qq = function(selector) {
        return (document.querySelectorAll(selector) || []);
    }

    Mock.Cookie = {
        set: function(name, value, days, path, domain) {
            days = days || "";
            if (days != '') {
                var date = new Date();
                date.setTime(date.getTime() + days * 24 * 60 * 60 * 1000);
                days = '; expires=' + date.toGMTString();
            }
            
            path = path || "/";
            if (path != '') {
                path = '; path=' + path;
            }
            
            if (domain) {
                domain = '; domain=' + domain;
            } else {
                domain = '';
            }
            document.cookie = name +"="+ encodeURIComponent(value) + days + path + domain;
            
            return value;
        },
        get: function(name) {
            var value = document.cookie.match('(?:^|;)\\s*' + name + '=([^;]*)');
            return value ? decodeURIComponent(value[1]) : null;
        },
        remove:	function(name) {
            var t = new Date();	t.setTime( t.getTime() - 24*3600*1000);
            document.cookie = name +"=; expires="+ t.toGMTString();
        }
    };

    // Extending Mock.
    Mock.GetHttpVariables = (function(){return(function(loc){var vars={};loc=(loc||window.location)+"";if(loc.contains("?")===true){var queryString=loc.substring(loc.indexOf("?")+1).split("&");for(var i=0,length=queryString.length,crr;i<length;i++){crr=[queryString[i].substringIndex("="),queryString[i].substringIndex("=",-1)];if(crr[0].substring((crr[0].length-2))==='[]'){if(typeof vars[crr[0].substring(0,(crr[0].length-2))]!=='object'){vars[crr[0].substring(0,(crr[0].length-2))]=[];};vars[crr[0].substring(0,(crr[0].length-2))].push(crr[1]);}else{crr.name=crr[0];crr.value=crr[1];vars[crr[0]]=crr[1];}}};vars.get=function(name){if(!name){return loc.substring(loc.indexOf("?")+1);};return this[name]||null;};return vars;});})();

    /**
     * Basic aJax function.
     *
     * @option string url.
     * @option object headers.
     * @option object gets.
     * @option object posts.
     * @option string method.
     *
     * @event onComplete().
     *
     * @update 2021/02/20 Ported from jCube.
     */
    Mock.Request = function(options) {
        let xhr        = new XMLHttpRequest(),
            onComplete = () => {},
            onError    = () => {};

        new Promise((resolve, reject) => {
            let contents = '',
                callback = resolve;

            if (typeof options === 'string') {
                options = {
                    url: options,
                };
            }

            if (options.contents !== undefined) {
                if (typeof options.contents === 'object') {
                    contents = JSON.stringify(options.contents);
                } else {
                    contents = options.contents;
                }
            }
            if (options.posts !== undefined) {
                Object.keys(options.posts).forEach(key => {
                    contents += "&"+ key +"="+ encodeURIComponent(options.posts[key]);
                });
            }

            if (options.gets !== undefined) {
                options.url += (options.url.indexOf("?") === -1) ? "?" : "&";
                Object.keys(options.gets).forEach(key => {
                    options.url += "&"+ key +"="+ encodeURIComponent(options.gets[key]);
                });
            }

            options.method = options.method || "GET";
            if (
                options.contents !== undefined ||
                options.posts !== undefined ||
                ['POST', 'UPDATE', 'PUT', 'PATCH'].contains(options.method.toUpperCase()) === true
            ) {
                options.method  = options.method !== 'GET' ? options.method : 'POST';
                options._method = options.method;
            }

            xhr.open(options.method.toUpperCase(), options.url);
            if (options.headers !== undefined) {
                Object.keys(options.headers).forEach(key => {
                    xhr.setRequestHeader(key, options.headers[key]);
                });
            }

            xhr.setRequestHeader('Content-type', (options.contentType || 'application/json'));

            for (var i in options) {
                if (xhr[i] === undefined) {
                    xhr[i] = options[i];
                }
            }
            xhr.onload = () => {
                let response = xhr.response;
                if ((xhr.getResponseHeader('content-type') || '').toLowerCase() === 'application/json') {
                    try {
                        response = JSON.parse(response);
                    } catch (e) {}
                }

                if (typeof options.onComplete === 'function') {
                    options.onComplete.call(xhr, response);
                }

                onComplete.call(xhr, response);
            };
            xhr.onerror = () => {
                let response = xhr.response;
                if ((xhr.getResponseHeader('content-type') || '').toLowerCase() === 'application/json') {
                    try {
                        json = JSON.parse(response);
                    } catch (e) {}
                }

                if (typeof options.onComplete === 'function') {
                    options.onComplete.call(xhr, response);
                }

                onError.call(xhr, xhr.response);
            };
            xhr.onabort = () => {
                if (typeof options.onAbort === 'function') {
                    options.onAbort.call(xhr, xhr.statusText);
                }
                onError.call(xhr, xhr.statusText);
            };

            xhr.send(contents);
        });

        xhr.then = function(resolve, reject) {
            onComplete = resolve  || (() => {});
            onError    = reject || (() => {});
            return xhr;
        };
        return xhr;
    };

	/**
	 * Carrega e valida de forma assincrona um script.
	 *
	 * @param array validations Format: [[object, str property, typeof]].
	 *
	 * @update 2021/02/20 Ported from jCube.
	 */
	Mock.whenReady = function(validations) {
		let promise = new Promise((resolve, reject) => {
			function Process(){
				var validated = true;
				for (var i = 0, crr, crrObj, _, length = validations.length; i < length; i++) {
					crr    = validations[i];
					crrObj = crr[0];

					if (crr[0][crr[1]] === undefined || typeof crr[0][crr[1]] !== crr[2]) {
						validated = false;
						break;
					}
				}

				if (validated === true) {
					resolve('Success');
				} else {
					window.setTimeout(function(){
						Process();
					}, 80);
				}
			}

			let asserted = true;
			if (validations instanceof Array === false) {
				reject('Argument 1 must be a type of array, having 3 arguments.');
				asserted = false;
			} else {
				for (let i = 0, crr, length = validations.length; i < length; i++) {
					crr = validations[i];
					if (crr[2] === undefined) {
						reject('Third argument of Mock.whenReady is undefined.');
						asserted = false;
						break;
					} else if (typeof crr[0] !== 'object' && typeof crr[0] !== 'function') {
						reject('First argument must be an object or a function.');
						asserted = false;
						break;
					} else if (typeof crr[1] !== 'string') {
						reject('Second argument of array must be a string.');
						asserted = false;
						break;
					} else if (typeof crr[2] !== 'string') {
						reject('Third argument of array must be a string.');
						asserted = false;
						break;
					}
				}
			}

			if (asserted === true) {
				Process();
			}
		});

		return promise;
	};

	Mock.load = (function(){
		function Load(file) {
            if (files[file] === undefined) {
                let suffix  = typeof window.assetsSuffix !== 'undefined' ? window.assetsSuffix : '.',
                    tag     = null;
                if (file.endsWith('.css') === true) {
                    tag = document.createElement('LINK');
                    tag.type = 'text/css';
                    tag.rel  = 'stylesheet';
                    tag.href = '/assets/css/'+ file.replace('.css', (typeof L === 'function' ? L.assetsSuffix : '.')+'css');
                } else {
                    tag = document.createElement('SCRIPT');
                    tag.type = 'text/javascript';
                    tag.src  = '/assets/js/'+ file.replace('.js', suffix + 'js');
                }

                tag.async = true;
                tag.defer = true;
                document.head.appendChild(tag);
                files[file] = true;
            }
		}

        let files = {};
		return (function(file, delay) {
			if (typeof delay === 'number') {
				window.setTimeout(function(){
					Load(file);
				}, delay);
			} else {
				Load(file);
			}
		});
	})();

    !(()=>{//Monkey patches (yeah, blame on me, IDC :P).
        Array.prototype.contains=function(obj,index){index=index<0?Math.max(this.length+index,0):0;for(var i=index||0,len=this.length;i<len;i++){if(this[i]===obj){return true}}return false};
        Array.prototype.isArray = true;
        Array.prototype.each=function(callback){
            for(var i=0,len=this.length;i<len;i++){
                callback.call(this[i],this[i],i,this)
            }
            return this;
        };
        String.prototype.contains=function(s,index){return this.indexOf(s,index||0)>-1};
        String.prototype.substringIndex=function(delim,n){var delimPos=this.indexOf(delim);n=n||0;if(delimPos==-1){return this+"";}if(n>-1){for(var i=1;i<n;i++){delimPos=this.indexOf(delim,delimPos+1);if(delimPos==-1){delimPos=this.length;break;}}return this.substring(0,delimPos);}else{var str=this;n=Math.abs(n);for(var i=0;i<n;i++){delimPos=str.lastIndexOf(delim);str=str.substring(0,delimPos);}delimPos++;return this.substring(delimPos+delim.length-1);}return this;};
        String.prototype.parseProperties = function(delimiter){
            delimiter = delimiter || ';';
        
            var options = {};
            this.split(delimiter).each(function(){
                var name  = this.substring(0, this.indexOf('=')).trim();
                var value = this.substring(this.indexOf('=') + 1).trim();
        
                if (name === '' && value.length > 0) {
                    options[value] = true;
                } else {
                    if (value === 'false') {
                        value = false;
                    } else if (value === 'true') {
                        value = true;
                    }
        
                    options[name] = value;
                }
            });
        
            return options;
        };
        HTMLElement.prototype.getParent = function(selector){
            if (selector) {
                var obj	= this;
                while ((obj = obj.parentNode) && obj.classList !== undefined) {
                    if (obj.classList === undefined) {
                        obj = null;
                        break;
                    }
        
                    if (selector.charAt(0) === '.' && obj.classList.contains(selector.substring(1)) === true) {
                        return obj;
                    } else if (selector.charAt(0) === '#' && (obj.id||'').split(' ').contains(selector.substring(1)) === true) {
                        return obj;
                    } else if (/^[a-z]/.test(selector.toLowerCase()) === true && obj.nodeName.toLowerCase().split(' ').contains(selector.toLowerCase()) === true) {
                        return obj;
                    } else if (/^[a-z]*\[(\w+)/.test(selector.toLowerCase()) === true) {
                        var reg = selector.match(/^([a-z]*)\[([a-zA-Z0-9\-_]+)/);
                        if (reg.length > 2 && typeof reg[2] === 'string') {
                            if ((reg[1].length === 0 || obj.nodeName.toLowerCase().split(' ').contains(reg[1].toLowerCase()) === true) && obj.getAttribute(reg[2]) !== null) {
                                return obj;
                            }
                        }
                    }
                }
        
                return null;
            }
            return this.parentNode;
        };
        HTMLElement.prototype.getNodeIndex = function() {
            var crr		= this;
            var index	= 0;
            while( crr && (crr = crr.previousElementSibling)) {
                index++;
            }
            return index;
        };
        HTMLElement.prototype.qq = function(selector) {
            return (this.querySelectorAll(selector) || []);
        };
        HTMLElement.prototype.q = function(selector) {
            return this.querySelector(selector);
        };
    })();

    return Mock;
})();
Queue.push(function(){// DATA-BUTTON-TOGGLE-SET: Deixa ativo os botões, de acordo com a qsa.
    let gets = Mock.GetHttpVariables();
    Mock.qq('[data-button-toggle-set]').forEach(item => {
        let name  = item.getAttribute('data-button-toggle-set'),
            isSet = gets[name] !== undefined;
        item.qq('a').forEach(eA=>{
            let href        = (eA.getAttribute('href') + '&').toLowerCase(),
                getVarValue = (gets[name] || '').toLowerCase();
            if (isSet === false) {
                if (href.contains(`?${name}=&`) === true) {
                    eA.classList.add('selected');
                } else {
                    eA.classList.remove('selected');
                }
            } else if (href.contains(`?${name}=${getVarValue}`) === true) {
                eA.classList.add('selected');
            } else if (isSet === true) {
                eA.classList.remove('selected');
            }
        });
    });
});

!function(){// START.
    !function(){// HOTKEYS NAVIGATION.
        function ScrollTo(key) {
            let h        = document.body.offsetHeight,
                y        = document.documentElement.scrollTop,
                eTabCard = Mock.qq('.card-tab')[Math.round(y/h)],
                eTab     = eTabCard.q('.card-tab__tab.active'),
                nextY    = null,
                delay    = 400;

            if (key === 40) {
                if ((y % h) === 0) {
                    nextY = (y + h);
                } else {
                    nextY = Math.ceil(y / h) * h;
                }
            } else if (key === 38) {
                if ((y % h) === 0) {
                    nextY = (y - h);
                } else {
                    nextY = Math.floor(y / h) * h;
                }
            } else if (key === 35) {
                nextY = document.body.scrollHeight;
                delay = 1000;
            } else if (key === 36) {
                nextY = 0;
                delay = 1000;
            } else if (key === 37) {
                if (eTab !== null) {
                    let eNextTab = eTab.previousElementSibling;
                    if (eNextTab !== null && eNextTab.getAttribute('data-actions') !== null) {
                        let options = eNextTab.getAttribute('data-actions').parseProperties()['card-tab'].parseProperties(',');
                        window.Actions["card-tab"].call(eNextTab, window.event, options);
                    }
                }
            } else if (key === 39) {
                if (eTab !== null) {
                    let eNextTab = eTab.nextElementSibling;
                    if (eNextTab !== null && eNextTab.getAttribute('data-actions') !== null) {
                        let options = eNextTab.getAttribute('data-actions').parseProperties()['card-tab'].parseProperties(',');
                        window.Actions["card-tab"].call(eNextTab, window.event, options);
                    }
                }
            }

            if (nextY !== null) {
                UpdatePageIndex();
                setTimeout(UpdatePageIndex, delay);
                window.scrollTo({
                    behavior: 'smooth',
                    top:      nextY,
                });
            }
        }
        function UpdatePageIndex() {
            let eIndexes = Mock.qq('.page-index'),
                total    = Mock.qq('#eLogs .card-tab').length,
                index    = (Math.round(document.documentElement.scrollTop / document.body.offsetHeight) + 1);

            eIndexes.forEach(item => {
                item.innerHTML = `${total > 0 ? index : 0} / ${total}`;
            });
        }
        
        document.addEventListener('mousewheel', e => {
            if (typeof window.path === 'string' && document.body.classList.contains('collapsed') === false) {
                if (e.wheelDeltaY < -60) {
                    let eTarget = e.target;

                    if ((eTarget.scrollTop + eTarget.offsetHeight + 30) > eTarget.scrollHeight) {
                        ScrollTo(40);
                    }
                } else if (e.wheelDeltaY > 60) {
                    if (e.target.scrollTop === 0) {
                        ScrollTo(38);
                    }
                }
            }
        });
        document.addEventListener('keydown', e => {
            if (['textarea', 'input', 'select'].indexOf(e.target.nodeName.toLowerCase()) > -1 ||
                e.altKey === true ||
                e.metaKey === true ||
                e.ctrlKey === true ||
                (e.shiftKey === true && e.keyCode !== 191) //Help
            ) {
                return;
            }

            let done = false;
            if (e.keyCode === 72) {// Home.
                window.location = '/';
                done = true;
            } else if (e.keyCode === 8) {// Back.
                window.location = '/';
                done = true;
            } else if (e.keyCode === 191) {// Help.
                console.log('Help on the way. Pls, be patiente!');
            } else if (typeof window.path === 'string') {// Log page.
                if ([35, 36, 37, 38, 39, 40].indexOf(e.keyCode) > -1) {// Arrows.
                    ScrollTo(e.keyCode);
                    done = true;
                } else if (e.keyCode === 46) {// Delete.
                    Actions['log-erase']();
                    done = true;
                } else if (e.keyCode === 19 || e.keyCode === 80) {// Pause key.
                    Actions['log-pause']();
                    done = true;
                }
            }

            if (done === true) {
                e.preventDefault();
                return;
            }
        });
        window.addEventListener('load', UpdatePageIndex);
    }();
}();
