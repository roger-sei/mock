<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<title>Mock</title>
		<link rel="stylesheet" href="/assets/css/main.css" >
		<link rel="stylesheet" href="/assets/css/card-tab.css" >
		<style>
			html {
				overflow: hidden;
			}
		</style>
		<script>
			var Actions = {},
				Queue   = [];
			window.lastInsertId = <?= $this->data['last-insert-id'] ?>;
			window.path         = '<?= $this->data['path'] ?>';
			window.idConfig     = '<?= $this->data['config']['id'] ?>';
			window.paused       = false;
		</script>
		<script async defer src="/assets/js/main.js" ></script>
		<script async defer src="/assets/js/actions.js" ></script>
		<script async defer src="/assets/js/card-tab.js" ></script>
		<script async defer src="/assets/js/log.js" ></script>
	</head>
	<body class="<?= (($_COOKIE['log-collapse'] ?? '') === 'collapsed' ? 'collapsed' : '' ) ?>" >
		<main>
			<nav class="toolbar" >
				<span class="toolbar__groups" >
					<a href="/" title="Go Home (H)" ><img src="/assets/img/home.png" alt="Icon home" width="38" ></a>
					<span class="toolbar__index page-index" >1/<?= $this->data['total-entries'] ?></span>
				</span>
				<span class="toolbar__groups" >
					<a data-actions="log-collapse::toggle" class="<?= ($_COOKIE['log-collapse'] ?? '') === 'collapsed' ? '' : 'inactive' ?>" ><img src="/assets/img/collapse.png" alt="Show collapsed log view" width="28" ></a>
					<a href="/settings/<?= $this->data['config']['id'] ?>/" ><img src="/assets/img/setup.png" alt="Setup rules" width="28" ></a>
					<span>&nbsp;</span>
					<a class="bt-pause" data-actions="log-pause" title="Stop/Resume listening for updates (P)" ><img src="/assets/img/pause.png" alt="Icon pause" width="24" data-src="pause=/assets/img/pause.png; resume=/assets/img/play.png" ></a>
					<a class="bt-erase" data-actions="log-erase" title="Delete this log (Del)" ><img src="/assets/img/delete.png" alt="Icon delete" width="22" ></a>
				</span>
			</nav>
			<div id="eLogs" >
				<?php foreach ($this->data['logs'] as $log) : ?>
				<section class='card-tab log-row' id="<?= $log['id']?>" >
					<div class="card-tab__tabs button-group--toggle margin-bottom--1em" >
						<a class="card-tab__tab active" data-actions="card-tab=open" >Raw</a>
						<a class="card-tab__tab" data-actions="card-tab=open,callback=ShowLogPreview" >Preview</a>
						<a class="card-tab__tab" data-actions="card-tab=open" >Response</a>
						<a class="card-tab__tab" data-actions="card-tab=open" >Headers</a>
						<a class="card-tab__tab" data-actions="card-tab=open" >Cookies</a>
						<a class="card-tab__tab" data-actions="card-tab=open" >Extra</a>
						<div class="time-info" data-time="<?= $log['tcreation'] ?>" ><?= str_replace('-', '/', $log['creation']) ?></div>
					</div>
					
					<div class="card-tab__cards" >
						<pre class="card-tab__card active log__response" ><?= htmlentities($log['body']) ?></pre>
						<pre class="card-tab__card" data-mime-type="<?= $log['mime_type'] ?>"></pre>
						<div class="card-tab__card" >
							<table cellspacing="0" cellpadding="0" border="0" >
								<tr><td>Mime type</td><td><?= $log['response']['mime_type'] ?></td></tr>
								<tr><td>HTTP Status</td><td><?= $log['response']['http_status'] ?></td></tr>
							</table>
							<pre class="border--pad" ><?= htmlentities($log['response']['body']) ?></pre>
						</div>
						<div class="card-tab__card" >
							<table cellspacing="0" cellpadding="0" border="0" >
								<?php foreach ($log['headers'] as $name => $value): ?>
								<tr class="header--<?= preg_replace('/[^a-z0-9_]+/', '-', strtolower($name)) ?>" ><td><?= $name ?></td><td><?= $value ?></td></tr>
								<?php endforeach ?>
							</table>
						</div>
						<div class="card-tab__card" >
							<table cellspacing="0" cellpadding="0" border="0" >
								<?php foreach ($log['cookies'] as $name => $value): ?>
								<tr class="cookie--<?= preg_replace('/[^a-z0-9_]+/', '-', strtolower($name)) ?>" ><td><?= htmlentities($name) ?></td><td><?= htmlentities($value) ?></td></tr>
								<?php endforeach ?>
							</table>
						</div>
						<div class="card-tab__card" >
							<table cellspacing="0" cellpadding="0" border="0" >
								<tr ><td>Content-type</td><td><?= $log['mime_type'] ?></td></tr>
								<tr ><td>Size</td><td><?= \Mock\Core\Util::integerToBytes($log['size'])?></td></tr>
								
								<?php if (empty($log['extra']) === false) : ?>
									<?php foreach ($log['extra'] as $name => $value): ?>
										<tr class="extra--<?= preg_replace('/[^a-z0-9_]+/', '-', strtolower($name)) ?>" ><td><?= htmlentities($name) ?></td><td><?= htmlentities($value) ?></td></tr>
									<?php endforeach ?>
								<?php endif ?>
								
								<tr ><td colspan="2" ><a class="link" href="/log/raw/<?= $log['id'] ?>/" >RAW</a></td></tr>
							</table>
							<br>
							
							<?php if (empty($log['gets']) === false) : ?>
								<table cellspacing="0" cellpadding="0" border="0" >
									<tr ><th colspan="2" >GETs</th></tr>
									<?php foreach ($log['gets'] as $name => $value): ?>
										<tr class="gets--<?= preg_replace('/[^a-z0-9_\-]+/', '-', strtolower($name)) ?>" ><td><?= htmlentities($name) ?></td><td><?= htmlentities($value) ?></td></tr>
									<?php endforeach ?>
								</table>
								<br>
							<?php endif ?>
								
							<?php if (empty($log['posts']) === false) : ?>
								<table cellspacing="0" cellpadding="0" border="0" >
									<tr ><th colspan="2" >POSTs</th></tr>
									<?php foreach ($log['posts'] as $name => $value): ?>
										<tr class="posts--<?= preg_replace('/[^a-z0-9_\-]+/', '-', strtolower($name)) ?>" ><td><?= htmlentities($name) ?></td><td><?= htmlentities($value) ?></td></tr>
									<?php endforeach ?>
								</table>
								<br>
							<?php endif ?>
						</div>
					</div>
				</section>
				<?php endforeach ?>
			</div>
		</main>
	</body>
</html>
