<?php

namespace Mock\Pages;


class Home extends \Mock\Core\Main
{
    

    /**
     * Exibe lista dos logs disponíveis.
     */
    public function __construct()
    {
        $this->data['logs'] = \Mock\Core\Database::q('logs.list');
    }


    /**
     * Verifica uma autorização básica, usando as credenciais roger@123456.
     * Em caso de erro, a aplicação morre.
     *
     * @return void.
     */
    private function checkAuthorization(): void
    {
        if (isset($_SERVER['PHP_AUTH_USER']) === false) {
            header('WWW-Authenticate: Basic realm="I am Batman ???"');
            header('HTTP/1.0 401 Unauthorized');
            echo 'User required for authentication :@';
            exit(1);
        }

        $headers = getallheaders();
        if (empty($headers['Authorization']) === true) {
            header('WWW-Authenticate: Basic realm="Integração B2B"');
            header('HTTP/1.0 401 User not sent in headers');
            echo 'Authorization not sent in headers :@';
            exit(2);
        }

        list($user, $pass) = explode(':' , base64_decode(substr($headers['Authorization'], 6)));
        if ($user !== 'roger') {
            header('WWW-Authenticate: Basic realm="Integração B2B"');
            header('HTTP/1.0 401 User not sent');
            echo 'Unknown user "'.$user.'". Please, use "roger" for tests purposes';
            exit(3);
        }

        if ($pass !== '123456') {
            header('WWW-Authenticate: Basic realm="Integração B2B"');
            header('HTTP/1.0 401 Invalid password');
            echo 'Invalid pass "'.$pass.'". Please, use "123456" for tests purposes';
            exit(4);
        }
    }


}
