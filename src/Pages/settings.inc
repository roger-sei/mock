<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<title>Mock</title>
		<link rel="stylesheet" href="/assets/css/main.css" >
		<link rel="stylesheet" href="/assets/css/colors.css" >
		<link rel="stylesheet" href="/assets/css/forms.css" >
		<script>
			var Actions = {},
				Queue   = [];
		</script>
		<style>
			.section__index {
				border-radius: 50%;
				color: blue;
				line-height: 2em;
				text-align: center;
			}
		</style>
		<script async defer src="/assets/js/main.js" ></script>
	</head>
	<body>
		<main>
			<nav class="toolbar" >
				<span class="toolbar__groups" >
					<a href="/" title="Go Home (H)" ><img src="/assets/img/home.png" alt="Icon home" width="32" ></a>
				</span>
				<span class="toolbar__groups" >
					<form action="/services/configs/rules/" method="post" data-forjax="showDialog; redirect=./" title="Create new rule" >
						<input type="hidden" name="id-config" value="<?= $this->data['id'] ?>" >
						<input type="hidden" name="rule" value="/.*/" >
						<input type="hidden" name="response" value="File received successfully!" >
						<input type="hidden" name="mime-type" value="text/plain" >
						<button class="button--reset display--block" type="submit" ><img src="/assets/img/new.png" alt="Icon add new rule" width="24" ></button>
					</form>
					<a href="/settings/json/<?= $this->data['id'] ?>/" title="Go JSON settings page" ><img src="/assets/img/json.png" alt="Icon JSON" width="22" ></a>
				</span>
			</nav>
			
			<div class="wrapper" >
				<div class="toolbar-vspace" ></div>
				
				<h1>
					<span>Settings for <em><?= $this->data['path'] ?></em></span>
				</h1>
				<?php for ($i = 0, $length = count($this->data['rules']); $i < $length; $i++) : ?>
				<section class="section transition--remove" >
					<div class="section__body" >
						<form action="/services/configs/rules/" method="patch" >
							<input type="hidden" name="id" value="<?= $this->data['rules'][$i]['id'] ?>" >
							<label class="form__text" >
								<span>Rule</span>
								<input type="text" name="rule" placeholder="/.*/" value="<?= $this->data['rules'][$i]['rule'] ?>" data-auto-update>
							</label>
							<label class="form__text" >
								<span>Mime Type</span>
								<input type="text" name="mime_type" placeholder="text/plain" value="<?= $this->data['rules'][$i]['mime_type'] ?>" data-auto-update>
							</label>
							<label class="form__text" >
								<span>HTTP Status</span>
								<input type="text" name="http_status" placeholder="HTTP/1.1 200 Ok" value="<?= $this->data['rules'][$i]['http_status'] ?>" data-auto-update>
							</label>
							<label class="form__text" >
								<span>Response</span>
								<textarea placeholder="<xml><success>Content received successfully!</success>" name="response" data-auto-update><?= $this->data['rules'][$i]['response'] ?></textarea>
							</label>
							
							<div class="justify-content--space-between" >
								<span class="section__index" ><?= ($i + 1) ?> </span>
								<a data-actions="erase-rule=id=<?= $this->data['rules'][$i]['id'] ?>" ><img src="/assets/img/delete.png" width="16" alt="Icon to erase this rule" ></a>
							</div>
						</form>
					</div>
				</section>
				<?php endfor ?>
			</div>
			
			<div class="height--2" ></div>
		</main>
	</body>
	<script async defer src="/assets/js/actions.js" ></script>
	<script async defer src="/assets/js/settings.js" ></script>
	<script async defer src="/assets/js/data/forjax.js" ></script>
	<script async defer src="/assets/js/data/auto-update.js" ></script>
</html>