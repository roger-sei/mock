<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<title>Mock</title>
		<?php include 'head.inc' ?>
		<style>
			body {
				background: url(/assets/img/wallpaper.jpg) repeat 0 0 / 2560px auto;
				color: #fff;
			}
			.cards {
				display: flex;
				flex: 1 0 8.333%;
				flex-flow: row wrap;
				padding: 1em;
			}
			.card {
				background: rgba(90,56,179, 0.8);
				color: #fff;
				display: inline-block;
				padding: 2em;
				margin: 1em;
				min-width: 180px;
				text-decoration: none;
			}
			.card > object {
				display: block;
				opacity: 0;
				transition: all 0.3s ease 0s;
			}
				.card:hover > object {
					opacity: 1;
				}
			.card > strong {
				display: block;
				margin-bottom: .5em;
			}
			.card > span {
				display: block;
			}
			.card__title {
				
			}
		</style>
		<script>
			window.lastInsertId = <?= ($this->data['logs'][0]['id'] ?? 0) ?>;
		</script>
		<script async defer src="/assets/js/home.js" ></script>
	</head>
	<body>
		<main>
			<section id="eLogs" >
				<div class="contents cards" >
					<?php foreach ($this->data['logs'] as $log) : ?>
					<a href="<?= $log['path'] ?>/" class="card" >
						<strong class="col--title" ><?= $log['path'] ?></strong>
						<span class="col--integer" ><?= $log['bytes'] ?></span>
						<span class="col--integer" ><?= $log['total'] . ' file' . ($log['total'] > 1 ? 's' : '') ?></span>
						<span class="col--date" data-time="<?= $log['tmodification'] ?>" ><?= $log['ellapsed'] ?></span>
						<!-- <object><a href="/settings/<?= $log['id'] ?>/" ><img src="/assets/img/setup.png" alt="Setup rules" width="32" ></a></object> -->
					</a>
					<?php endforeach ?>
				</div>
			</section>
		</main>
	</body>
</html>