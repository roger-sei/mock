<?php

namespace Mock\Pages\Log;


class Raw extends \Mock\Core\Main
{
    

    /**
     * Show a raw log.
     *
     * @param string $id Id's log, as in mock.logs.id.
     */
    public function __construct(string $id)
    {
        $this->data = \Mock\Core\Database::q(
            'logs.get',
            [
                'id' => $id
            ],
            0
        );

        $mime = ($this->data['mime_type'] ?? 'text/plain');
        header("Content-type: $mime");
        header('HTTP/1.1 200 Ok');
    }


}
