<?php

namespace Mock\Pages;


class Settings extends \Mock\Core\Main
{
    

    /**
     * Show the Rules page.
     *
     * @param string $id Config ID.
     */
    public function __construct(string $id = '')
    {
        \Mock\Core\Main::dieIfFail(\Mock\Core\Util::regExp($id, '^(\d+)$') !== $id, 'Invalid ID!');
        \Mock\Core\Main::dieIfFail(empty($id), 'Log ID cannot be empty!');

        $config = \Mock\Core\Database::q('configs.get', ['id' => $id], 0);
        $this->on404(empty($config), "/rules/$id/");
        $this->data = $config;
        $this->data['rules'] = \Mock\Core\Database::q('configs.rules', ['id_config' => $id]);;
    }


}
