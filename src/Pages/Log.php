<?php

namespace Mock\Pages;


class Log extends \Mock\Core\Main
{
    

    /**
     * Exibe lista dos logs disponíveis.
     *
     * @param string $path Path of log, as saved in config.
     */
    public function __construct(string $path)
    {
        \Mock\Core\Main::dieIfFail(\Mock\Core\Util::regExp($path, '^([a-zA-Z0-9_\-\.\/]+)$') !== $path, 'Invalid name');

        $this->data['path'] = $path;

        $contents = file_get_contents('php://input');

        if (empty($contents) === false
            || empty($_POST) === false
            || in_array(strtolower($_REQUEST['method'] ?? ''), ['post', 'put', 'delete', 'patch', 'update']) === true
        ) {
            $results = \Services\Logs::post($path, $contents);

            header($results['response']['http_status']);
            header("Content-type: {$results['response']['mime_type']}");
            die($results['response']['body']);
        } elseif (isset($_GET['erase']) === true) {
            $results = \Services\Logs::delete($path);
            $this->finish($results);
        } elseif (isset($_GET['check']) === true) {
            $results = \Services\Logs::getSize($path, (int) ($_GET['size'] ?? '0'));
            $this->finish($results);
        } else {
            // This controller.
        }

        $config = \Mock\Core\Database::q('SELECT id, path, config FROM config WHERE path = :path', ['path' => $path], 0);
        $this->on404(empty($config), $path);
        $logs = \Mock\Core\Database::q('logs.get-by-id-config', ['id_config' => $config['id']]);
        $size = 0;

        foreach ($logs as $log) {
            $size += $log['size'];
        }

        $this->data['config']         = $config;
        $this->data['path']           = $path;
        $this->data['logs']           = $logs;
        $this->data['last-insert-id'] = ($logs[0]['id'] ?? 0);
        $this->data['size']           = $size;
        $this->data['total-entries']  = count($logs);
    }


}
