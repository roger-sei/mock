<!DOCTYPE html>
<html lang="pt-br">
	<head>
		<title>Mock</title>
		<link rel="stylesheet" href="/assets/css/main.css" >
		<link rel="stylesheet" href="/assets/css/colors.css" >
		<link rel="stylesheet" href="/assets/css/forms.css" >
		<script>
			var Actions = {},
				Queue   = [];
		</script>
		<style>
			.form__text textarea {
				font-size: .75em;
				height: calc(100vh - 23em);
			}
		</style>
		<script async defer src="/assets/js/main.js" ></script>
	</head>
	<body>
		<main>
			<nav class="toolbar" >
				<span class="toolbar__groups" >
					<a href="/" title="Go Home (H)" ><img src="/assets/img/home.png" alt="Icon home" width="32" ></a>
				</span>
				<span class="toolbar__groups" >
					<a href="/settings/json/" title="Go JSON settings page" ><img src="/assets/img/json.png" alt="Icon JSON" width="22" ></a>
				</span>
			</nav>
			
			<div class="wrapper" >
				<div class="toolbar-vspace" ></div>
				
				<h1>
					<span>Rules in JSON</span>
				</h1>
				<section class="section" >
					<div class="section__body" >
						<form action="/services/configs/json/" method="patch" >
							<input type="hidden" name="id" value="<?= $this->data['id'] ?>" >
							<label class="form__text" >
								<textarea name="json" data-auto-update><?= htmlentities(json_encode($this->data['json'], JSON_UNESCAPED_SLASHES | JSON_PRETTY_PRINT)) ?></textarea>
							</label>
						</form>
					</div>
				</section>
			</div>
			
			<div class="height--2" ></div>
		</main>
	</body>
	<script async defer src="/assets/js/actions.js" ></script>
	<script async defer src="/assets/js/data/auto-update.js" ></script>
</html>