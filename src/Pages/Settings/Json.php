<?php

namespace Mock\Pages\Settings;


class Json extends \Mock\Core\Main
{
    

    /**
     * Show the Rules page.
     *
     * @param string $id Config ID.
     */
    public function __construct(string $id = '')
    {
        \Mock\Core\Main::dieIfFail(\Mock\Core\Util::regExp($id, '^(\d+)$') !== $id, 'Invalid ID!');

        if (empty($id) === true) {
            $filter = [];
        } else {
            $filter = [
                'required-fields' => [
                    'c.id' => $id,
                ]
            ];
        }

        $rules = \Mock\Core\Database::q('rules.all', $filter);
        $json  = [];
        foreach ($rules as $rule) {
            $json[$rule['path']] = ($json[$rule['path']] ?? []);
            $json[$rule['path']][] = [
                'rule'        => $rule['rule'],
                'response'    => $rule['response'],
                'mime_type'   => $rule['mime_type'],
                'http_status' => $rule['http_status'],
            ];
        }

        $this->data['id']   = $id;
        $this->data['json'] = $json;
    }


}
