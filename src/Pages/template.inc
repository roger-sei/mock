<section class='card-tab log-row log-row-section' >
	<div class="card-tab__tabs button-group--toggle margin-bottom--1em" >
		<a class="card-tab__tab active" data-actions="card-tab=open" >Raw</a>
		<a class="card-tab__tab" data-actions="card-tab=open,callback=ShowLogPreview" >Preview</a>
		<a class="card-tab__tab" data-actions="card-tab=open" >Headers</a>
		<a class="card-tab__tab" data-actions="card-tab=open" >Cookies</a>
		<a class="card-tab__tab" data-actions="card-tab=open" >Extra</a>
		<div class="time-info" ><?= $crr['creation'] ?></div>
	</div>
	
	<div class="card-tab__cards" >
		<pre class="card-tab__card active log__response" ><?= htmlentities($crr['contents']) ?></pre>
		<pre class="card-tab__card" data-mime-type="<?= $crr['mime-type'] ?>" ></pre>
		<div class="card-tab__card" >
			<table cellspacing="0" cellpadding="0" border="0" >
				<?php foreach ($crr['headers'] as $headerName => $headerValue) : ?>
				<tr class="header--<?= strtolower($headerName) ?>" ><td><?= $headerName ?></td><td><?= $headerValue ?></td></tr>
				<?php endforeach ?>
			</table>
		</div>
		<div class="card-tab__card" >
			<table cellspacing="0" cellpadding="0" border="0" >
				<?php foreach ($crr['cookies'] as $cookieName => $cookieValue) : ?>
				<tr><td><?= $cookieName ?></td><td><?= $cookieValue ?></td></tr>
				<?php endforeach ?>
			</table>
		</div>
		<div class="card-tab__card" >
			<table cellspacing="0" cellpadding="0" border="0" >
				<tr><td>IP</td><td><?= $_SERVER['REMOTE_ADDR'] ?></td></tr>
			</table>
		</div>
	</div>
</section>
