#config: json=config; time-ellapsed=ellapsed
SELECT
    id,
	path,
	size,
	total,
	config,
	DATE_FORMAT(modification, '%Y/%m/%d') as modification,
	UNIX_TIMESTAMP(modification) AS tmodification,
	(UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(modification)) AS ellapsed
FROM
	config
WHERE
	id = :id