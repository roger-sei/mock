SELECT
    id,
    id_config,
	rule,
	response,
	mime_type,
	http_status
FROM
	config_rules
WHERE
	id_config = :id_config
ORDER BY
	id DESC
