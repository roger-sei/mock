SELECT
    id,
	path,
	size,
	total,
	config,
	modification
ORDER BY
	id DESC
LIMIT
	1