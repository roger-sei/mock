#config: json=response, headers, cookies, gets, posts, extra; time-ellapsed=ellapsed; bytes=bytes
SELECT
	id,
	id_config,
	body,
	response,
	headers,
	cookies,
	gets,
	posts,
	extra,
	size,
	size AS bytes,
	mime_type,
	creation,
	DATE_FORMAT(creation, '%Y/%m/%d') as creation,
	UNIX_TIMESTAMP(creation) AS tcreation,
	(UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(creation)) AS ellapsed
FROM
	logs l
WHERE
	id = :id
