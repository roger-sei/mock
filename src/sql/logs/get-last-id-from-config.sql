SELECT
	l.id
FROM
	logs l
WHERE
	1 = 1
	AND l.id_config = :id_config
	AND l.id > :id
ORDER BY
	l.id DESC
LIMIT
	1
