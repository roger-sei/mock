#config: json=config; time-ellapsed=ellapsed; bytes=bytes
SELECT
	c.id,
	c.path,
	c.config,
	c.size,
	c.size AS bytes,
	c.total,
	DATE_FORMAT(c.modification, '%Y/%m/%d') as modification,
	UNIX_TIMESTAMP(c.modification) AS tmodification,
	(UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(c.modification)) AS ellapsed
FROM
	config c
WHERE
	c.size > 0
ORDER BY
	c.id DESC
LIMIT
	100