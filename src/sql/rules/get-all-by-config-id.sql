SELECT
	id,
	id_config,
	rule,
	rule,
	response,
	mime_type,
	http_status
FROM
	config_rules
WHERE
	id_config = :id_config
ORDER BY
	RAND() DESC
LIMIT
	100