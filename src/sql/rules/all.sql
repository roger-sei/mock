SELECT
    cr.id,
	cr.id_config,
    c.path,
    cr.rule,
    cr.response,
    cr.mime_type,
    cr.http_status
FROM
	config c
    LEFT JOIN config_rules cr ON c.id = cr.id_config
WHERE
	1 = 1
    AND cr.id_config IS NOT NULL