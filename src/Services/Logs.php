<?php

namespace Services;

use \Mock\Core\{
    Database,
    Main,
    Util
};

class Logs extends \Mock\Core\Services
{

    /**
     * Deletes a log.
     *
     * @param integer $idConfig Rule ID.
     *
     * @return array.
     */
    public static function delete(string $idConfig = null): array
    {
        $idConfig = ($idConfig ?? $_REQUEST['id-config'] ?? self::$args['id-config'] ?? '');
        Main::dieIfFail(empty($idConfig), 'Missing "id-config" field.');
        Main::dieIfFail(Util::regExp($idConfig, '^\d+$') !== $idConfig, 'Invalid format for "id-config" variable.');

        $config = Database::q('configs.get', ['id' => $idConfig], 0);
        Main::dieIfFail(empty($config), 'Log not found or already deleted.');

        $affecteds = Database::d(
            '
                UPDATE
                    config
                SET
                    size  = 0,
                    total = 0
                WHERE
                    id = :id
            ',
            ['id' => $idConfig]
        );
        $affecteds += Database::d(
            '
                DELETE FROM
                    logs
                WHERE
                    id_config = :id_config
            ',
            ['id_config' => $idConfig]
        );

        return [
            'success'  => 'Log deleted successfully',
            'affected' => $affecteds,
        ];
    }


    /**
     * Return a list of all logs.
     *
     * @return array.
     */
    public static function get(): array
    {
        $results = Database::q('logs');

        return [
            'success' => true,
            'results' => $results,
        ];
    }


    /**
     * Saves a log.
     *
     * @param string $path     Path of log, as saved in config.
     * @param string $contents Content sent.
     *
     * @return array.
     */
    public static function post(string $path = null, $contents = null): array
    {
        Main::dieIfFail(empty($path), '$_GET["path"] is required.');
        Main::dieIfFail(Util::regExp($path, '^([a-zA-Z0-9_\-\.\/]+)$') !== $path, 'Invalid path.');

        $headers  = getallheaders();
        $mimeType = 'text/plain';
        foreach ($headers AS $name => $value) {
            if (strtolower(trim($name)) === 'content-type') {
                $mimeType = strtolower(trim($value));
                break;
            }
        }

        $contents = ($contents ?? file_get_contents('php://input'));
        $config   = Database::q('configs.get-by-path', ['path' => $path], 0);
        if (empty($config)) {
            Database::i(
                'INSERT INTO config (path, config) VALUES (:path, :config)',
                [
                    'path'   => $path,
                    'config' => json_encode([]),
                ]
            );
            $config = Database::q('SELECT id, path, config FROM config WHERE path = :path', ['path' => $path], 0);
        }

        $rules = Database::q('rules.get-all-by-config-id', ['id_config' => $config['id']]);
        $rule  = [];
        foreach ($rules as $crr) {
            if (preg_match($crr['rule'], $contents) === 1) {
                $rule = $crr;
                break;
            }
        }

        $response = [
            'body'        => ($rule['response'] ?? 'Success'),
            'http_status' => ($rule['http_status'] ?? 'HTTP/1.1 200 Ok'),
            'mime_type'   => 'text/plain',
        ];

        $response = self::checkHooks($path, $contents, $response);

        $gets = [];
        foreach (($_GETS ?? []) as $name => $value) {
            if (gettype($value) !== 'string') {
                $value = print_r($value, true);
            }

            $gets[$name] = $value;
        }

        $posts = [];
        foreach (($_POSTS ?? []) as $name => $value) {
            if (gettype($value) !== 'string') {
                $value = print_r($value, true);
            }

            $posts[$name] = $value;
        }

        $insertId = Database::i(
            'INSERT INTO
                logs (
                    id_config,
                    body,
                    response,
                    headers,
                    cookies,
                    gets,
                    posts,
                    extra,
                    size,
                    mime_type
                ) VALUES (
                    :id_config,
                    :body,
                    :response,
                    :headers,
                    :cookies,
                    :gets,
                    :posts,
                    :extra,
                    :size,
                    :mime_type
                )
            ',
            [
                'id_config' => $config['id'],
                'body'      => $contents,
                'response'  => json_encode($response),
                'headers'   => json_encode($headers),
                'cookies'   => json_encode($_COOKIE ?? []),
                'gets'      => json_encode($gets),
                'posts'     => json_encode($posts),
                'extra'     => json_encode([
                    'ip' => $_SERVER['REMOTE_ADDR'],
                ]),
                'size'      => strlen($contents),
                'mime_type' => $mimeType,
            ]
        );

        $insertId = Database::u(
            '
            UPDATE
                config
            SET
                size  = (size + :size),
                total = (total + 1)
            WHERE
                id = :id_config
            ',
            [
                'id_config' => $config['id'],
                'size'      => strlen($contents),
            ]
        );

        return [
            'insert-id' => $insertId,
            'response'  => $response,
            'success'   => 'Log saved successfully',
        ];
    }



    /**
     * Checks for a hooks's existence. If a path matchs a hook, it will be executed.
     * Response and Content are provided, so a hook can check it's contents and
     * change the response accordingly.
     *
     * @param string $path     URL path.
     * @param string $contents Request body.
     * @param array  $response Array with response, http_status and mime_type fields.
     *
     * @return array The given $response, modified by a hook script.
     */
    private static function checkHooks(string $path, string $contents, array $response): array
    {
        $parts     = explode('/', $path);
        $hooksPath = ROOT . '/hooks/';
        while (empty($parts) === false) {
            $hook = $hooksPath . join('/', $parts) . '.php';
            if (file_exists($hook) === true) {
                include $hook;
            }

            array_pop($parts);
        }

        return $response;
    }


}
