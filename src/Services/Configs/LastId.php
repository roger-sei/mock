<?php

namespace Services\Configs;

class LastId extends \Mock\Core\Services
{


    /**
     * Returns Last log entry ID.
     *
     * @param integer $idConfig Config identifier.
     *
     * @return integer The last ID found.
     */
    public static function get(int $idConfig = null): array
    {
        $idConfig = ($idConfig ?? $_REQUEST['id-config'] ?? self::$args['id-config'] ?? null);
        \Mock\Core\Main::dieIfFail(isset($idConfig) === false, 'Missing "id-config".');
        \Mock\Core\Main::dieIfFail(\Mock\Core\Util::regExp($idConfig, '^\d*$') !== $idConfig, 'Invalid id format.');

        $count        = 0;
        $lastIdConfig = $idConfig;
        while ($count < 120) {
            $lastIdConfig = (int) \Mock\Core\Database::q('SELECT id FROM config WHERE size > 0 ORDER BY id DESC LIMIT 1', [], 0, 'id');
            if ($lastIdConfig > $idConfig) {
                break;
            }

            $count++;
            usleep(100000);
            clearstatcache();
        }

        return [
            'lastInsertId' => $lastIdConfig,
            'success'      => true,
        ];
    }


}
