<?php

namespace Services\Configs;

use \Mock\Core\{
    Main,
    Util
};

class Json extends \Mock\Core\Services
{


    /**
     * Updates an existing rules, using a json file.
     *
     * @param integer $id    Rule ID.
     * @param string  $field Field to updated. Currently, only json is allowed.
     * @param string  $value Settings in json format.
     *
     * @return array.
     */
    public static function patch(int $id = null, string $field = null, string $value = null): array
    {
        $id    = ($id ?? $_REQUEST['id'] ?? self::$args['id'] ?? null);
        $value = ($value ?? $_REQUEST['value'] ?? self::$args['value'] ?? '');

        Main::dieIfFail(empty($value), 'Missing "json" parameter.');

        $json      = json_decode($value, true);
        $affecteds = 0;
        foreach ($json as $path => $properties) {
            $config = \Mock\Core\Database::q('configs.get-by-path', ['path' => $path], 0);
            if (empty($config) === true) {
                $idConfig = \Mock\Core\Database::i(
                    'INSERT INTO config (path, config) VALUES (:path, :config)',
                    [
                        'path'   => $path,
                        'config' => json_encode([]),
                    ]
                );
                
                $config = \Mock\Core\Database::q('configs.get', ['id' => $idConfig], 0);
            }

            $affecteds += \Mock\Core\Database::d(
                "
                    DELETE FROM
                        config_rules
                    WHERE
                        id_config = :id_config
                ",
                [
                    'id_config' => $config['id'],
                ],
            );
            foreach ($properties as $property) {
                $insertId = \Mock\Core\Database::i(
                    "
                        INSERT INTO
                            config_rules (
                                id_config,
                                rule,
                                response,
                                mime_type,
                                http_status
                            ) VALUES (
                                :id_config,
                                :rule,
                                :response,
                                :mime_type,
                                :http_status
                            )
                    ",
                    [
                        'id_config'   => $config['id'],
                        'rule'        => ($property['rule'] ?? '/.*/'),
                        'response'    => ($property['response'] ?? 'Success'),
                        'mime_type'   => ($property['mime_type'] ?? 'text/plain'),
                        'http_status' => ($property['http_status'] ?? '200 Ok'),
                    ],
                );

                $insertId++;
            }
        }

        return [
            'affecteds' => $affecteds,
            'success'  => 'Settings imported successfully',
        ];
    }


}
