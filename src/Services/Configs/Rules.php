<?php

namespace Services\Configs;

use \Mock\Core\{
    Main,
    Util
};

class Rules extends \Mock\Core\Services
{


    /**
     * Deletes a rule.
     *
     * @param integer $id    Rule ID.
     *
     * @return array.
     */
    public static function delete(int $id = null): array
    {
        $id = ($id ?? $_REQUEST['id'] ?? self::$args['id'] ?? null);
        Main::dieIfFail(empty($id), 'Missing "id" field.');
        Main::dieIfFail(Util::regExp($id, '^\d+$') !== $id, 'Invalid id format.');

        $rule = \Mock\Core\Database::q('rules.get-by-id', ['id' => $id]);
        Main::dieIfFail(empty($rule), 'Rule not found.');
        $affecteds = \Mock\Core\Database::d(
            '
                DELETE FROM config_rules WHERE id = :id
            ',
            ['id' => $id]
        );

        return [
            'rule'      => $rule,
            'success'   => 'Rule removed successfully',
            'affecteds' => $affecteds,
        ];
    }


    /**
     * Updates part of saved config_rule.
     *
     * @param integer $id    Rule ID.
     * @param string  $field Field name in config_rule. Allowed fields: rule, response, mime_type.
     * @param string  $value Value.
     *
     * @return array.
     */
    public static function patch(int $id = null, string $field = null, string $value = null): array
    {
        $id    = ($id ?? $_REQUEST['id'] ?? self::$args['id'] ?? null);
        $field = ($field ?? $_REQUEST['field'] ?? self::$args['field'] ?? '');
        $value = ($value ?? $_REQUEST['value'] ?? self::$args['value'] ?? '');

        Main::dieIfFail(empty($id), 'Missing "id" field.');
        Main::dieIfFail(empty($field), 'Missing "field" parameter.');
        Main::dieIfFail(empty($value), 'Missing "value" parameter.');

        $alloweds = [
            'rule',
            'response',
            'mime_type',
            'http_status',
        ];
        Main::dieIfFail(Util::regExp($id, '^\d+$') !== $id, 'Invalid id format.');
        Main::dieIfFail(in_array($field, $alloweds) === false, "Rule \"$field\" field not allowed to change.");

        if ($field === 'rule') {
            Main::dieIfFail(@preg_match($value, 'teste') === false, 'Invalid regular expression for rule field.');
        }

        $affecteds = \Mock\Core\Database::u(
            "
                UPDATE
                    config_rules
                SET
                    $field = :value
                WHERE
                    id = :id
            ",
            [
                'id'    => $id,
                'value' => $value,
            ],
        );

        return [
            'affecteds' => $affecteds,
            'success'  => 'Rule updated successfully',
        ];
    }


    /**
     * Inserts a new Rule in the actual config.
     *
     * @param integer $idConfig Config identifier.
     * @param string  $rule     Regex rule.
     * @param string  $response The response text.
     * @param string  $mimeType The mime type.
     *
     * @return array {last-insert-id, success, error}.
     */
    public static function post(int $idConfig = null, string $rule = null, string $response = null, string $mimeType = 'text/plain'): array
    {
        $idConfig = ($idConfig ?? $_REQUEST['id-config'] ?? self::$args['id-config'] ?? null);
        $rule     = ($rule ?? $_REQUEST['rule'] ?? self::$args['rule'] ?? '');
        $response = ($response ?? $_REQUEST['response'] ?? self::$args['response'] ?? '');
        $mimeType = ($mimeType ?? $_REQUEST['mime-type'] ?? self::$args['mime-type'] ?? '');

        Main::dieIfFail($idConfig === null, 'Missing "id-config" field.');
        Main::dieIfFail(empty($rule), 'Missing "rule" field.');
        Main::dieIfFail(empty($response), 'Missing "response" field.');

        Main::dieIfFail(Util::regExp($idConfig, '^\d+$') !== $idConfig, 'Invalid id format.');
        Main::dieIfFail(str_starts_with($rule, '/') === false || str_ends_with($rule, '/') === false, 'The field "rule" does not look like a regular expression.');
        Main::dieIfFail(preg_match($rule, 'teste') === false, 'The regular expression for "rule" field is not a valid PCRE compliance.');
        Main::dieIfFail(Util::regExp($mimeType, '^([a-zA-Z0-9\/\-\.]+)$') !== $mimeType, 'Invalid mime-type.');

        $config = \Mock\Core\Database::q('configs.get', ['id' => $idConfig], 0);
        Main::dieIfFail(empty($config), 'Config not found', null, 401);
        $insertId = \Mock\Core\Database::i(
            '
                INSERT INTO
                    config_rules(
                        id_config,
                        rule,
                        response,
                        mime_type
                    ) VALUES (
                        :id_config,
                        :rule,
                        :response,
                        :mime_type
                    )
            ',
            [
                'id_config' => $idConfig,
                'rule'      => $rule,
                'response'  => $response,
                'mime_type' => $mimeType,
            ]
        );

        return [
            'insertId' => $insertId,
            'success'  => 'Rule added successfully',
        ];
    }


}
