<?php

namespace Services;

class Projects extends \Mock\Core\Main
{


    /**
     * Returns a list of logs in the actual project.
     *
     * @param string $project Project's name.
     *
     * @return array.
     */
    public static function get(string $project = null, bool $detailed = false, bool $sortBySize = false): array
    {
        $project    = ($project ?? $_REQUEST['project'] ?? '');
        $detailed   = ($detailed ?? isset($_REQUEST['detailed']) ?? false);
        $sortBySize = ($sortBySize ?? isset($_REQUEST['sort-by-size']) ?? false);

        if ($detailed === true) {
            $result = self::getListDetailed($project);
        } else {
            $result = self::getList($project, $sortBySize);
        }

        return $result;
    }


    /**
     * Returns a basic list of availables logs in this project.
     *
     * @param string $project Project's name.
     *
     * @return array.
     */
    public static function getList(string $project): array
    {
        \Mock\Core\Main::dieIfFail(empty($project), 'Missing param "project".');
        \Mock\Core\Main::dieIfFail(\Mock\Core\Util::regExp($project, '^([a-zA-Z0-9_\-]+)$') !== $project, 'Invalid project name');

        if (file_exists("/tmp/mock-logs/$project/") === true) {
            return array_diff(scandir("/tmp/mock-logs/$project/"), ['.', '..']);
        }

        return [];
    }
    



    /**
     * Returns a detailed list of available logs in this project.
     *
     * @param string  $project    Project's name.
     * @param boolean $sortBySize Sort by size. Default: false.
     *
     * @return array.
     */
    public static function getListDetailed(string $project, bool $sortBySize = false): array
    {
        \Mock\Core\Main::dieIfFail(empty($project), 'Missing param "project".');
        \Mock\Core\Main::dieIfFail(\Mock\Core\Util::regExp($project, '^([a-zA-Z0-9_\-]+)$') !== $project, 'Invalid project name');

        if (file_exists("/tmp/mock-logs/$project/") === true) {
            $result = @shell_exec("ls -l --full-time /tmp/mock-logs/$project/") . PHP_EOL;
            $logs   = [];
            preg_match_all('/(\d+) +(\d{4}-\d{2}-\d{2} +\d{2}:\d{2}:\d{2})\.\d+ \+\d+ +([a-zA-Z0-9\-_]+)\n/', $result, $matches);
            for($i = 0, $length = count($matches[0]); $i < $length; $i++) {
                $modification  = str_replace('-', '/', $matches[2][$i]);
                $tmodification = strtotime($modification);
                $diff          = (time() - strtotime($modification));
                if (($t = ($diff / (60 * 60 * 24 * 30))) > 1) {
                    $ellapsed = ($t < 2 ? "1 month" : floor($t) . ' months' );
                } elseif (($t = ($diff / (60 * 60 * 24))) > 1) {
                    $ellapsed = ($t < 2 ? "1 day" : floor($t) . ' days' );
                } elseif (($t = ($diff  / (60 * 60))) > 1) {
                    $ellapsed = ($t < 2 ? "1 hour" : floor($t) . ' hours' );
                } elseif (($t = ($diff / (60))) > 1) {
                    $ellapsed = ($t < 2 ? "1 minute" : floor($t) . ' minutes' );
                } else {
                    $ellapsed = $diff . ' seconds';
                }

                $tsize = (int) $matches[1][$i];
                if ($tsize > pow(1024, 3)) {
                    $size = number_format($tsize / pow(1024, 3), 2) . ' GB';
                } elseif ($tsize > pow(1024, 2)) {
                    $size = number_format($tsize / pow(1024, 2), 2) . ' MB';
                } elseif ($tsize > 1024) {
                    $size = number_format($tsize / 1024, 2) . ' KB';
                } else {
                    $size = "$tsize B";
                }

                $logs[] = [
                    'name'          => $matches[3][$i],
                    'tsize'         => $tsize,
                    'size'          => $size,
                    'modification'  => $modification,
                    'tmodification' => strtotime($modification),
                    'ellapsed'      => $ellapsed,
                ];
            }

            if ($sortBySize === true) {
                usort($logs, function($a, $b){
                    return ($b['tsize'] - $a['tsize']);
                });
            } else {
                usort($logs, function($a, $b){
                    return ($b['tmodification'] - $a['tmodification']);
                });
            }

            return $logs;
        }

        return [];
    }
    

}
