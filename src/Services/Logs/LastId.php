<?php

namespace Services\Logs;

class LastId extends \Mock\Core\Services
{


    /**
     * Returns Last log entry ID.
     *
     * @param integer $idConfig ID from mock.config.
     * @param integer $idLog    ID from mock.log.
     *
     * @return integer The last ID found.
     */
    public static function get(int $idConfig = null, int $idLog = null): array
    {
        $idConfig = ($idConfig ?? $_REQUEST['id-config'] ?? self::$args['id-config'] ?? null);
        $idLog    = ($idLog ?? $_REQUEST['id-log'] ?? self::$args['id-log'] ?? null);
        \Mock\Core\Main::dieIfFail(isset($idConfig) === false, 'Missing "id-config".');
        \Mock\Core\Main::dieIfFail(isset($idLog) === false, 'Missing "id-log".');

        $count  = 0;
        $idLast = $idLog;
        while ($count < 120) {
            $idLast = \Mock\Core\Database::q(
                'logs.get-last-id-from-config',
                [
                    'id_config' => $idConfig,
                    'id'        => $idLog,
                ],
                0,
                'id'
            ) ?: 0;

            if ($idLast > $idLog) {
                break;
            }

            $count++;
            usleep(100000);
            clearstatcache();
        }

        return [
            'lastInsertId' => ($idLast ?: 0),
            'success'      => true,
        ];
    }


}
